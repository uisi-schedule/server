import Dependencies._

name := "schedule"

scalaVersion in ThisBuild := "2.13.1"
scalacOptions in ThisBuild += "-Ymacro-annotations"

libraryDependencies in ThisBuild ++= Seq("com.olegpy" %% "better-monadic-for" % "0.3.1").map(compilerPlugin)

lazy val domian = project
  .settings(name := "domian")
  .settings(
    libraryDependencies ++= derevo.core ::
    derevo.tschema ::
    derevo.circe ::
    circe.core ::
    circe.parser ::
    circe.generic ::
    circe.config ::
    enumeratum.core ::
    enumeratum.circe :: newtype :: Nil
  )

lazy val parser = project
  .settings(name := "parser", version := "0.1.2-SNAPSHOT")
  .dependsOn(domian)
  .enablePlugins(BuildInfoPlugin, LauncherJarPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "space.littleinferno.schedule.parser"
  )
  .settings(
    testFrameworks += new TestFramework("verify.runner.Framework"),
    libraryDependencies ++=
      circe.core ::
      circe.parser ::
      circe.generic ::
      circe.config ::
      osLib ::
      apacheIO ::
      apache.poi ::
      apache.`poi-ooxml` ::
      parboiled ::
      scalajHttp ::
      derevo.core ::
      derevo.tschema ::
      derevo.circe ::
      enumeratum.core ::
      enumeratum.circe ::
      cats.core ::
      cats.effect ::
      zio.core ::
      zio.cats ::
      newtype ::
      clist.core ::
      clist.macros :: Nil
  )

lazy val server = project
  .settings(name := "server", version := "0.5.0-rc4")
  .dependsOn(domian)
  .enablePlugins(sbtdocker.DockerPlugin, JavaAppPackaging)
  .settings(dockerSettings)
  .configs(IntegrationTest)
  .settings(
    libraryDependencies ++=
      circe.core ::
      circe.parser ::
      circe.generic ::
      circe.config ::
      derevo.core ::
      derevo.tschema ::
      derevo.circe ::
      enumeratum.core ::
      enumeratum.circe ::
      typedSchedma ::
      jose.core ::
      jose.circe ::
      doobie.core ::
      doobie.postgres ::
      swaggerUI ::
      bcrypt ::
      cats.core ::
      cats.effect ::
      zio.core ::
      zio.cats ::
      newtype ::
      apache.poi ::
      apache.`poi-ooxml` ::
      akkaCirce :: Nil
  )
  .settings(
    Defaults.itSettings,
    libraryDependencies ++= Seq(
      Dependencies.testcontainers,
      Dependencies.postgresql,
      Dependencies.flyway,
      Dependencies.hikari,
      "org.scalatest" %% "scalatest" % "3.1.0" % "it, test"
    )
  )

lazy val dockerSettings = Seq(
  imageNames in docker := {
    val tag =
      git.gitCurrentTags.value.headOption.getOrElse(git.gitCurrentBranch.value)
    Seq(ImageName(namespace = Some("space.littleinferno"), repository = "schedule-backend", tag = Some(tag)))
  },
  dockerfile in docker := {
    val appDir: File = stage.value
    val targetDir    = s"/app/${name.value}"

    new Dockerfile {
      from("openjdk:12")
      copy(appDir, targetDir)
      workDir(targetDir)
      cmd(s"$targetDir/bin/${executableScriptName.value}")
    }
  }
)

resolvers in ThisBuild ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("public"),
  Resolver.sonatypeRepo("snapshots")
)
