package space.littleinfenro

import cats.effect.{ContextShift, IO}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.util.transactor.Transactor
import doobie.util.transactor.Transactor.Aux
import io.circe.generic.extras.Configuration
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.configuration.{ClassicConfiguration, Configuration}
import org.scalatest.{AsyncFunSuite, BeforeAndAfter, BeforeAndAfterAll}
import org.testcontainers.containers.PostgreSQLContainer

import scala.concurrent.ExecutionContext

class Test extends AsyncFunSuite with BeforeAndAfter with BeforeAndAfterAll  {
import Test._

  implicit val transactor: Transactor.Aux[IO, Unit] = Test.transactor

  override def afterAll(): Unit = {
    dataSource.close()
    postgres.stop()
  }
}

object Test {
  private lazy val postgres = {
    println("sdgsdg")
    val postgres = new PostgreSQLContainer()
    postgres.start()
    postgres
  }

  private lazy val config = {
    val config = new HikariConfig()
    config.setJdbcUrl(postgres.getJdbcUrl)
    config.setUsername(postgres.getUsername)
    config.setPassword(postgres.getPassword)
    config
  }

  private val dataSource = {
    Class.forName(postgres.getDriverClassName)

    val dataSource = new HikariDataSource(config)

    val conf       = new ClassicConfiguration()
    conf.setDataSource(dataSource)
    val flyway = new Flyway(conf)

    flyway.migrate()
    dataSource
  }

  private implicit val context: ContextShift[IO] = IO.contextShift(ExecutionContext.global)


  lazy val transactor: Aux[IO, Unit] = Transactor
    .fromDriverManager[IO]("org.postgresql.Driver", postgres.getJdbcUrl, postgres.getUsername, postgres.getPassword)
}
