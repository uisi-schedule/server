package space.littleinferno.schedule.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import space.littleinferno.schedule.server.exporter.ExcelExport
import space.littleinferno.schedule.server.modules.{AuthModule, GroupModule, InfoModule, LessonModule, ScheduleModule, TeacherModule}
import space.littleinferno.schedule.server.repositories.Repositories
import space.littleinferno.schedule.server.schema.Schema
import space.littleinferno.schedule.server.services.{AuthServiceImpl, GroupServiceImpl, InfoServiceImpl, LessonServiceImpl, ScheduleServiceImpl, TeacherServiceImpl}

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {

    implicit val system: ActorSystem    = ActorSystem()
    implicit val mat: ActorMaterializer = ActorMaterializer()

    for {
      config                                  <- config.config.AppConfig.load()
      implicit0(tokenProvider: TokenProvider) <- IO(new TokenProvider(config.token))
      repos  = new Repositories[IO](config.database)
      export = new ExcelExport[IO]
      modules = new ScheduleModule(new ScheduleServiceImpl[IO](repos.scheduleRepository, export)) ::
      new InfoModule(new InfoServiceImpl()) ::
      new AuthModule(new AuthServiceImpl[IO](tokenProvider, repos.userRepository)) ::
      new GroupModule(new GroupServiceImpl[IO](repos.groupRepository)) ::
      new LessonModule(new LessonServiceImpl[IO](repos.lessonRepository)) ::
      new TeacherModule(new TeacherServiceImpl[IO](repos.teacherRepository)) :: Nil

      _ <- config.bootstrap.fold(0.pure[IO]) { credentials =>
        repos.userRepository.create(credentials)
      }
      _ <- new Server(config.server, new Schema(modules)).run
      _ <- IO(println("server started"))
    } yield ExitCode.Success

  }
}
