package space.littleinferno.schedule.server.repositories

import java.sql.Date
import java.time.{LocalDate, LocalTime}

import doobie.Meta
import doobie.postgres.implicits._
import doobie.util.Read
import doobie.implicits.javatime._
import doobie.util.invariant.{NullableCellRead, NullableCellUpdate}
import io.estatico.newtype.Coercible
import space.littleinferno.schedule.domian.predef.{Auditorium, LessonType, ScheduleKind, TimeSlot}
import space.littleinferno.schedule.server.domain.GraduateLevel

trait MetaInstances {
  implicit val dateArray: (Meta[Array[LocalDate]]) = {
    val raw = Meta.Advanced.array[Date]("date", "_date")
    def checkNull[B >: Null](a: Array[B], e: Exception): Array[B] =
      if (a == null) null else if (a.contains(null)) throw e else a
    (
      raw
        .timap(checkNull(_, NullableCellRead))(checkNull(_, NullableCellUpdate))
        .timap(_.map(_.toLocalDate))(_.map(Date.valueOf)),
      )
  }

  implicit val auditoriumRead: Read[Auditorium] =
    Read[(String, Int)].map { case (x, y) => Auditorium(x, y) }

  implicit val timeSlotRead: Read[TimeSlot] =
    Read[(LocalTime, LocalTime, Option[Int])].map { case (begin, end, number) => TimeSlot(begin, end, number) }
}

trait EnumInstances {
  implicit def lessonTypeMeta: Meta[LessonType] =
    pgEnumString[LessonType]("lesson_type", LessonType.withName, _.entryName)

  implicit def scheduleKindMeta: Meta[ScheduleKind] =
    pgEnumString[ScheduleKind]("schedule_kind", ScheduleKind.withName, _.entryName)

  implicit def graduateLevelMeta: Meta[GraduateLevel] =
    pgEnumString[GraduateLevel]("graduate_level", GraduateLevel.withName, _.entryName)
}

trait NewTypeInstances {
  implicit def newTypeDoobieMeta[R, N](implicit ev: Coercible[Meta[R], Meta[N]], R: Meta[R]): Meta[N] =
    ev(R)
}
