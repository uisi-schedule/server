package space.littleinferno.schedule.server.services

import space.littleinferno.schedule.domian.domian.Session
import space.littleinferno.schedule.server.domain.{Group, ScheduleShortInfo}
import space.littleinferno.schedule.server.repositories.{GroupRepository, ScheduleRepository}

object GroupService {
  type Service[F[_]] = SimpleRestService[F, Int, Group]
}

class GroupServiceImpl[F[_]](repo: GroupRepository[F]) extends GroupService.Service[F] {
  def list: F[List[Group]] = repo.list

  def create(session: Session, body: Group): F[Group] = repo.create(body)

  def edit(session: Session, id: Int, body: Group): F[Group] = repo.edit(id, body)

  def delete(session: Session, id: Int): F[Unit] = repo.delete(id)
}
