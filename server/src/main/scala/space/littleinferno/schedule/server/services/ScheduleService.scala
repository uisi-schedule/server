package space.littleinferno.schedule.server.services

import java.time.LocalDate

import cats.ApplicativeError
import cats.effect.Sync
import cats.implicits._
import space.littleinferno.schedule.domian.domian.Session
import space.littleinferno.schedule.domian.predef.{EditScheduleAction, ScheduleFormat, ScheduleKind}
import space.littleinferno.schedule.server.domain.{ScheduleByGroupFormat, ScheduleByTeacherFormat, ScheduleShortInfo}
import space.littleinferno.schedule.server.exporter.Exporter
import space.littleinferno.schedule.server.repositories.ScheduleRepository
import space.littleinferno.schedule.server.tschema.instances.Excel

trait ScheduleService[F[_]] {
  def add(session: Session, schedule: ScheduleFormat): F[Int]

  def edit(session: Session, id: Int, body: List[EditScheduleAction]): F[Unit]

  def delete(session: Session, id: Int): F[Unit]

  def publish(session: Session, id: Int): F[Unit]

  def get(id: Int): F[ScheduleFormat]

  def byTeacher(name: String, from: LocalDate, to: LocalDate): F[ScheduleByTeacherFormat]

  def byGroup(name: String, from: LocalDate, to: LocalDate): F[ScheduleByGroupFormat]

  def schedules(name: String): F[List[ScheduleShortInfo]]

  def export(id: Int): F[Excel]
}

class ScheduleServiceImpl[F[_]: Sync](repo: ScheduleRepository[F], exporter: Exporter[F])(
    implicit F: ApplicativeError[F, Throwable]
) extends ScheduleService[F] {
  def add(session: Session, schedule: ScheduleFormat): F[Int] =
    schedule.kind match {
      case ScheduleKind.Draft =>
        repo.addSchedule(schedule)
      case ScheduleKind.Actual =>
        for {
          id <- repo.addSchedule(schedule.copy(kind = ScheduleKind.Draft))
          _  <- repo.publish(id)
        } yield id
      case ScheduleKind.Archive =>
        F.raiseError(new IllegalArgumentException("invalid schedule kind  = Archive"))
    }

  def edit(session: Session, id: Int, body: List[EditScheduleAction]): F[Unit] = repo.editSchedule(id, body)

  def delete(session: Session, id: Int): F[Unit] = repo.deleteSchedule(id)

  def publish(session: Session, id: Int): F[Unit] = repo.publish(id)

  def get(id: Int): F[ScheduleFormat] =
    repo.getFullSchedule(id)

  def byTeacher(name: String, from: LocalDate, to: LocalDate): F[ScheduleByTeacherFormat] =
    repo.getScheduleByTeacher(name, from, to)

  def byGroup(name: String, from: LocalDate, to: LocalDate): F[ScheduleByGroupFormat] =
    repo.getScheduleByGroup(name, from, to)

  def export(id: Int): F[Excel] =
    repo.getFullSchedule(id).flatMap(exporter.export)

  def schedules(name: String): F[List[ScheduleShortInfo]] = repo.getShortInfos(name)
}
