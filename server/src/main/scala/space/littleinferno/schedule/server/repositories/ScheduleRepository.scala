package space.littleinferno.schedule.server.repositories

import java.time.LocalDate

import scala.jdk.CollectionConverters._

import cats.effect.Sync
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.traverse._
import doobie.ConnectionIO
import doobie.free.connection
import doobie.implicits._
import doobie.util.fragment.Fragment
import doobie.util.transactor.Transactor
import space.littleinferno.schedule.domian.predef._
import space.littleinferno.schedule.server.domain._
import space.littleinferno.schedule.server.repositories.implicits._

class ScheduleRepository[F[_]: Sync](implicit xa: Transactor.Aux[F, Unit]) {
  import ScheduleRepository._

  def addSchedule(schedule: ScheduleFormat): F[Int] =
    (schedule.kind match {
      case ScheduleKind.Draft =>
        for {
          id <- addDraft(schedule.group)
          _  <- schedule.lessons.traverse(addLessons(id, _))
        } yield id
      case kind =>
        connection.raiseError[Int](new Error(s"invalid schedule kind = $kind"))
    }).transact(xa)

  def editSchedule(scheduleId: Int, body: List[EditScheduleAction]): F[Unit] =
    (for {
      kind <- sql"select kind from schedule where id = $scheduleId".query[ScheduleKind].unique
      _ <- connection
        .raiseError[Int](new Error("You can't edit archive or actual schedule"))
        .whenA(kind != ScheduleKind.Draft)
      _ <- body.traverse {
        case EditScheduleAction.Add(lesson) =>
          addLessons(scheduleId, lesson).void
        case EditScheduleAction.Edit(id, lesson) =>
          for {
            _ <- sql"delete from schedule_lessons  where schedule_id = $scheduleId and id = $id".update.run
            _ <- addLessons(scheduleId, lesson)
          } yield ()
        case EditScheduleAction.Delete(id) =>
          sql"delete from schedule_lessons  where schedule_id = $scheduleId and id = $id".update.run.void
      }
    } yield ()).transact(xa)

  def deleteSchedule(id: Int): F[Unit] =
    sql"delete from schedule where id = $id".update.run.transact(xa).void

  private def addDraft(group: String): ConnectionIO[Int] =
    sql"select add_schedule_draft($group)".query[Int].unique

  private def addLessons(scheduleId: Int, lesson: LessonFormat): ConnectionIO[Unit] =
    for {
      timeSlotId <- sql"""insert into time_slots (slot_begin, slot_end, number) 
                          values (${lesson.timeSlot.begin}, ${lesson.timeSlot.end}, ${lesson.timeSlot.number})
                          on conflict(slot_begin, slot_end) 
                          do update set slot_begin=excluded.slot_begin
                          returning id"""
        .query[Int]
        .unique
      auditoriumId <- sql"""insert into auditorium (room, building)
                          values (${lesson.auditorium.number}, ${lesson.auditorium.building})
                          on conflict(room, building) 
                          do update set room=excluded.room
                          returning id"""
        .query[Int]
        .unique
      _ <- sql"""select add_lesson($scheduleId, ${lesson.name}, ${lesson.lessonType}, ${lesson.subGroup},
                         ${lesson.teacher}, ${lesson.dates.toArray}, $timeSlotId, $auditoriumId)"""
        .query[Unit]
        .unique
    } yield ()

  def publish(group: Int): F[Unit] =
    sql"select publish_schedule($group)".query[Unit].unique.transact(xa)

  def getScheduleByGroup(group: String, begin: LocalDate, end: LocalDate): F[ScheduleByGroupFormat] =
    getSchedule(group, begin, end)(selectDayByGroup)((a, b) => ScheduleByGroupFormat(a, b)).transact(xa)

  def getScheduleByTeacher(teacher: String, begin: LocalDate, end: LocalDate): F[ScheduleByTeacherFormat] =
    getSchedule(teacher, begin, end)(selectDayByTeacher)((a, b) => ScheduleByTeacherFormat(a, b)).transact(xa)

  def getShortInfos(group: String): F[List[ScheduleShortInfo]] =
    sql"""select id, group_name, kind, start_date, end_date
          from schedule
          where group_name = $group
          order by id desc
          """.query[ScheduleShortInfo].to[List].transact(xa)

  def getFullSchedule(id: Int): F[ScheduleFormat] =
    (for {
      info <- sql"""select id, group_name, kind, start_date, end_date from schedule where id = $id""".query[ScheduleShortInfo].unique
      lessons <- sql"""select schedule_lessons.id, name, dates, lesson_type, sub_group, teacher, room, building, slot_begin, slot_end, number
                       from schedule_lessons
                       left join (select id, slot_begin, slot_end, number from time_slots) as slot
                                    on schedule_lessons.time_slot_id = slot.id
                       left join (select id, room, building from auditorium) as aud
                                    on schedule_lessons.auditorium_id = aud.id
                       where schedule_id = ${info.id}"""
        .query[LessonFormat]
        .to[List]
    } yield ScheduleFormat(info.group, lessons, info.kind)).transact(xa)
}

object ScheduleRepository {
  private def getSchedule[V, A <: ScheduleGetFormat](value: V, begin: LocalDate, end: LocalDate)(
      d: (V, LocalDate) => ConnectionIO[Day]
  )(v: (V, List[Day]) => A): ConnectionIO[A] =
    begin
      .datesUntil(end.plusDays(1))
      .iterator
      .asScala
      .toList
      .traverse(d(value, _))
      .map(v(value, _))

  private def selectDayByGroup(group: String, date: LocalDate): ConnectionIO[Day] =
    for {
      lessons <- sql"""select * from day_by_group($group, $date)"""
        .query[SingleLessonByGroupFormat]
        .to[List]
    } yield
      Day(
        date,
        lessons
          .groupBy(lesson => (lesson.timeSlot, lesson.subGroup.isDefined))
          .flatMap {
            case ((timeSlot, true), xs) =>
              MultipleLessonByGroupFormat(timeSlot, xs) :: Nil
            case ((_, _), xs) =>
              xs
          }
          .toList
          .sortBy(_.timeSlot.begin)
      )

  private def selectDayByTeacher(teacher: String, date: LocalDate): ConnectionIO[Day] =
    sql"""select * from day_by_teacher($teacher, $date)""".query[LessonByTeacherFormat].to[List].map(Day(date, _))
}
