package space.littleinferno.schedule.server.services

import cats.effect.Sync
import space.littleinferno.schedule.domian.domian.Session
import space.littleinferno.schedule.server.domain.Lesson
import space.littleinferno.schedule.server.repositories.LessonRepository

object LessonService {
  type Service[F[_]] = SimpleRestService[F, Int, Lesson]
}

class LessonServiceImpl[F[_]: Sync](repo: LessonRepository[F]) extends LessonService.Service[F] {
  def list: F[List[Lesson]] = repo.list

  def create(session: Session, body: Lesson): F[Lesson] = repo.create(body)

  def edit(session: Session, id: Int, body: Lesson): F[Lesson] = repo.edit(id, body)

  def delete(session: Session, id: Int): F[Unit] = repo.delete(id)
}
