package space.littleinferno.schedule.server.repositories

object implicits
  extends EnumInstances with MetaInstances with NewTypeInstances with doobie.util.meta.MetaConstructors
  with doobie.util.meta.MetaInstances with doobie.util.meta.TimeMetaInstances with doobie.postgres.Instances
