package space.littleinferno.schedule.server.repositories

import cats.effect.Sync
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.traverse._
import doobie.implicits._
import doobie.util.transactor.Transactor
import space.littleinferno.schedule.server.domain.Teacher

trait TeacherRepository[F[_]] {
  def list: F[List[Teacher]]

  def create(teacher: Teacher): F[Teacher]

  def edit(id: Int, teacher: Teacher): F[Teacher]

  def delete(id: Int): F[Unit]
}

class TeacherRepositoryImpl[F[_]: Sync](implicit xa: Transactor.Aux[F, Unit]) extends TeacherRepository[F] {
  def list: F[List[Teacher]] =
    sql"select name, id from teachers"
      .query[Teacher]
      .to[List]
      .transact(xa)

  def create(teacher: Teacher): F[Teacher] =
    sql"""insert into teachers (name)
          values (${teacher.name})
          returning name, id"""
      .query[Teacher]
      .unique
      .transact(xa)

  def edit(id: Int, teacher: Teacher): F[Teacher] =
    sql"""update teachers set name = ${teacher.name}
          where id = $id
          returning name, id"""
      .query[Teacher]
      .unique
      .transact(xa)

  def delete(id: Int): F[Unit] =
    sql"delete from teachers where id = $id".update.run
      .transact(xa)
      .void
}
