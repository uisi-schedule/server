package space.littleinferno.schedule.server.exporter
import java.io.{ByteArrayOutputStream, FileOutputStream}
import java.time.LocalDate
import java.time.temporal.TemporalField
import io.estatico.newtype.ops._
import cats.effect.Sync
import space.littleinferno.schedule.server.domain
import space.littleinferno.schedule.server.domain.{ScheduleByGroupFormat, ScheduleByTeacherFormat}
import org.apache.poi.ss.usermodel.{BorderStyle, CellStyle, CellType, CreationHelper, HorizontalAlignment, IndexedColors, PrintSetup, VerticalAlignment, Workbook}
import org.apache.poi.xssf.usermodel.{XSSFCell, XSSFCellStyle, XSSFRow, XSSFSheet, XSSFWorkbook}
import cats.implicits._
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xwpf.usermodel.VerticalAlign
import space.littleinferno.schedule.domian.predef.{Auditorium, LessonFormat, ScheduleFormat}
import space.littleinferno.schedule.server.tschema.instances.Excel

import scala.collection.mutable

class ExcelExport[F[_]: Sync] extends Exporter[F] {
  override def export(ss: ScheduleFormat): F[Excel] ={

    val workbook = new XSSFWorkbook

    // Create a Sheet
    val sheet = workbook.createSheet(ss.group)

    sheet.getPrintSetup.setPaperSize(PrintSetup.A3_PAPERSIZE)

    val cs = workbook.createCellStyle()

    val font = workbook.createFont()

    font.setFontHeight(10)
    font.setFontName("Times New Roman")

    cs.setWrapText(true)
    cs.setVerticalAlignment(VerticalAlignment.CENTER)
    cs.setFont(font)

    implicit val celcs: XSSFCellStyle = workbook.createCellStyle()
    celcs.cloneStyleFrom(cs)

    celcs.setBorderBottom(BorderStyle.THIN)
    celcs.setBorderTop(BorderStyle.THIN)
    celcs.setBorderLeft(BorderStyle.MEDIUM)
    celcs.setBorderRight(BorderStyle.MEDIUM)

    val header = sheet.getHeader

    val sh = new SheetHelper(sheet)

    val rotated = workbook.createCellStyle()
    rotated.cloneStyleFrom(celcs)

    sh.mergedCol()
      .cell("Уральский технический институт связи и информатики")
      .mergeCol(4)
      .row
      .mergedCol()
      .cell("(филиал)  федерального государственного бюджетного образовательного учреждения высшего образования")
      .mergeCol(4)
      .row
      .mergedCol()
      .cell(
        "\"Сибирский государственный университет телекоммуникаций и информатики\" в г. Екатеринбурге  (УрТИСИ  СибГУТИ)"
      )
      .mergeCol(4)
      .row
      .row
      .cell("дни")
      .cell("№ пары")
      .cell(ss.group)
      .cell("ауд.")
      .row

    import java.time.format.DateTimeFormatter
    val formatter = DateTimeFormatter.ofPattern("dd.MM.YY")

    def datesShow(dates: List[LocalDate]): String =
      if (dates.size < 3)
        dates.map(formatter.format(_)).mkString(", ")
      else {
        ///TODO Really need?
        val ranges = dates.tail.foldLeft(TimeRange(dates.head, dates.head) :: Nil) {
          case a @ (TimeRange(begin, end) :: tail, i) =>
            if (end.plusDays(7) == i)
              TimeRange(begin, i) :: tail
            else
              TimeRange(i, i) :: a._1
        }
        ranges.reverse
          .map {
            case TimeRange(begin, end) if begin == end =>
              formatter.format(begin)
            case TimeRange(begin, end) =>
              s"с ${formatter.format(begin)} по ${formatter.format(end)}"
          }
          .mkString(", ")
      }

    def lessonFormatShow(format: LessonFormat): String =
      format.name + "; " + datesShow(format.dates) + "; " + format.lessonType + format.subGroup.fold("")(
        " - подгруппа " + _
      ) + "; " + format.teacher

    ss.lessons.groupBy(a => a.dates.head.getDayOfWeek.ordinal).toList.sortBy(_._1).foreach {
      case (i, value) =>
        sh.mergedRow()
        value.groupBy(b => b.timeSlot.begin).foreach {
          case (j, v) =>
            sh.mergedRow(1)

            v.foreach { s =>
              sh.cell(i.toString)
                .updateStyle(rotated)
                .cell(j.toString)
                .cell(lessonFormatShow(s))
                .cell(s"${s.auditorium.number} УК№${s.auditorium.building}")
                .row
            }
            sh.mergeRow()
        }
        sh.mergeRow()
    }

    sheet.autoSizeColumn(2)

    val output = new ByteArrayOutputStream()
    workbook.write(output)
    output.close()
    output.toByteArray.coerce[Excel].pure[F]
  }

}

case class TimeRange(begin: LocalDate, end: LocalDate)

class SheetHelper(sheet: XSSFSheet) {

  private var currentRow: XSSFRow = sheet.createRow(0)

  // col, row
  private val mergedBegin: mutable.Stack[(Int, Int)] = mutable.Stack((0, 0))

  private var lastRow  = 0
  private var lastCell = -1

  def mergedRow(col: Int = lastCell + 1): SheetHelper = {
    mergedBegin.push((col, lastRow))
    this
  }

  def mergedCol(row: Int = lastRow): SheetHelper = {
    mergedBegin.push((lastCell + 1, row))
    this
  }

  def mergeRow(rows: Int = lastRow): SheetHelper = {
    val (col, row) = mergedBegin.pop()
    if (row + 2 < rows)
      sheet.addMergedRegion(new CellRangeAddress(row, rows - 1, col, col))

    this
  }

  def mergeCol(cols: Int = lastCell): SheetHelper = {
    val (col, row) = mergedBegin.pop()

    if (col + 2 < cols)
      sheet.addMergedRegion(new CellRangeAddress(row, row, col, cols - 1))

    this
  }

  def cell(value: String)(implicit style: XSSFCellStyle): SheetHelper = {
    lastCell += 1
    val cell = currentRow.createCell(lastCell)
    cell.setCellValue(value)
    cell.setCellStyle(style)
    this
  }

  def cell(value: Int)(implicit style: XSSFCellStyle): SheetHelper = {
    lastCell += 1
    val cell = currentRow.createCell(lastCell)
    cell.setCellValue(value)
    cell.setCellStyle(style)
    this
  }

  def row: SheetHelper = {
    currentRow = sheet.createRow(lastRow + 1)
    lastRow += 1
    lastCell = -1
    this
  }

  def updateStyle(fa: XSSFCellStyle): SheetHelper = {
    val cell = currentRow.getCell(lastCell)
    cell.setCellStyle(fa)
    this
  }
}
