package space.littleinferno.schedule.server.modules

import cats.effect.IO
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._
import space.littleinferno.schedule.domian.domian.{Credentials, Token, TokenPair}
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.RegisterInfo
import space.littleinferno.schedule.server.services.AuthService
import space.littleinferno.schedule.server.tschema.instances._

class AuthModule(service: AuthService[IO])(implicit tokenProvider: TokenProvider) extends Module {
  import AuthModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object AuthModule {
  private def api =
    tagPrefix('auth) |>
    (registerToken <> register <> login)

  private def registerToken =
    get |>
    session |>
    keyPrefix('registerToken) :>
    $$[Token]

  private def register =
    post |>
    keyPrefix('register) :>
    body[RegisterInfo]('registerInfo) |>
    $$[TokenPair]

  private def login =
    post |>
    keyPrefix('login) :>
    body[Credentials]('credentials) |>
    $$[TokenPair]

}
