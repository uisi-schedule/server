package space.littleinferno.schedule.server.services

import cats.effect.IO
import cats.syntax.applicative._
import space.littleinferno.schedule.server.domain.ServerInfo

trait InfoService {
  def health: IO[ServerInfo]
}

class InfoServiceImpl() extends InfoService {
  def health: IO[ServerInfo] =
    ServerInfo(true).pure[IO]
}
