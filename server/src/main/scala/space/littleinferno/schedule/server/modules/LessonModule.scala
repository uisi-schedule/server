package space.littleinferno.schedule.server.modules

import cats.effect.IO
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.{Group, Lesson}
import space.littleinferno.schedule.server.services.LessonService.Service
import space.littleinferno.schedule.server.tschema.instances._

class LessonModule(service: Service[IO])(implicit tokenProvider: TokenProvider) extends Module {
  import LessonModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object LessonModule {
  private def api =
    tagPrefix("lessons") |>
    (lessonList <> createLesson <> editLesson <> deleteLesson)

  private def lessonList =
    get |>
    key("list") |>
    $$[List[Lesson]]

  private def createLesson =
    post |>
    session |>
    key("create") |>
    reqBody[Lesson] |>
    $$[Lesson]

  private def editLesson =
    put |>
    session |>
    key("edit") |>
    capture[Int]("id") |>
    reqBody[Lesson] |>
    $$[Lesson]

  private def deleteLesson =
    delete |>
    session |>
    key("delete") |>
    capture[Int]("id") |>
    $$[Unit]

}
