package space.littleinferno.schedule.server.modules

import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._

import space.littleinferno.schedule.domian.predef.ScheduleFormat
import space.littleinferno.schedule.server.domain.{ScheduleByGroupFormat, ScheduleByTeacherFormat, ServerInfo}
import space.littleinferno.schedule.server.services.{InfoService, ScheduleService}
import space.littleinferno.schedule.server.tschema.instances._

class InfoModule(service: InfoService) extends Module {
  import InfoModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object InfoModule {
  private def api =
    tag('Info) |>
    prefix('info) |>
    health

  def health =
    get |>
    keyPrefix('health) :>
    $$[ServerInfo]

}
