package space.littleinferno.schedule.server.modules

import java.time.LocalDate

import cats.effect.IO
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._
import space.littleinferno.schedule.domian.predef.{EditScheduleAction, ScheduleFormat}
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.{ScheduleByGroupFormat, ScheduleByTeacherFormat, ScheduleShortInfo}
import space.littleinferno.schedule.server.services.ScheduleService
import space.littleinferno.schedule.server.tschema.instances._

class ScheduleModule(service: ScheduleService[IO])(implicit tokenProvider: TokenProvider) extends Module {

  import ScheduleModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object ScheduleModule {
  private def api =
    tag("schedules") |>
    (addSchedule <> editSchedule <> deleteSchedule <> getSchedule <> publish <> export <> getSchedules <> byGroup <> byTeacher)

  private def addSchedule =
    post |>
    session |>
    prefix("schedules") |>
    key("add") |>
    body[ScheduleFormat]("schedule") |>
    $$[Int]

  private def editSchedule =
    put |>
    session |>
    prefix("schedules") |>
    key("edit") |>
    capture[Int]("id") |>
    reqBody[List[EditScheduleAction]] |>
    $$[Unit]

  private def deleteSchedule =
    delete |>
    session |>
    prefix("schedules") |>
    key("delete") |>
    capture[Int]("id") |>
    $$[Unit]

  private def getSchedule =
    get |>
    prefix("schedules") |>
    key("get") |>
    capture[Int]("id") |>
    $$[ScheduleFormat]

  private def getSchedules =
    get |>
    prefix("groups") |>
    capture[String]("name") |>
    keyPrefix("schedules") |>
    $$[List[ScheduleShortInfo]]

  private def byGroup =
    get |>
    prefix("groups") |>
    capture[String]("name") |>
    prefix("schedule") |>
    key("byGroup") |>
    queryParam[LocalDate]("from") |>
    queryParam[LocalDate]("to") |>
    $$[ScheduleByGroupFormat]

  private def byTeacher =
    get |>
    prefix("teachers") |>
    capture[String]("name") |>
    prefix("schedule") |>
    key("byTeacher") |>
    queryParam[LocalDate]("from") |>
    queryParam[LocalDate]("to") |>
    $$[ScheduleByTeacherFormat]

  private def publish =
    post |>
    session |>
    prefix("schedules") |>
    capture[Int]("id") |>
    keyPrefix("publish") |>
    $$[Unit]

  private def export =
    get |>
    prefix("schedules") |>
    capture[Int]("id") |>
    keyPrefix("export") |>
    $$[Excel]

}
