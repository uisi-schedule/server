package space.littleinferno.schedule.server.repositories

import cats.effect.Sync
import com.github.t3hnar.bcrypt._
import doobie.implicits._
import doobie.util.transactor.Transactor

import space.littleinferno.schedule.domian.domian.Credentials
import space.littleinferno.schedule.domian.predef.Login
import space.littleinferno.schedule.server.repositories.implicits._

class UserRepository[F[_]: Sync](implicit xa: Transactor.Aux[F, Unit]) {
  def create(credentials: Credentials): F[Int] =
    sql"""insert into users(login, password) 
          values (${credentials.login}, ${credentials.password.bcrypt(generateSalt)})""".update.run
      .transact(xa)

  def contains(login: Login): F[Boolean] =
    sql"""select exists(select 1 from users where login = $login)"""
      .query[Boolean]
      .unique
      .transact(xa)

  def checkCredentials(credentials: Credentials): F[Boolean] =
    sql"""select login, password from users where login = ${credentials.login}"""
      .query[Credentials]
      .option
      .map(_.exists(cred => credentials.password.isBcrypted(cred.password)))
      .transact(xa)
}
