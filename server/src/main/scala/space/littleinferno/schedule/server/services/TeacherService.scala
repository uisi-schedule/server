package space.littleinferno.schedule.server.services

import cats.effect.Sync
import space.littleinferno.schedule.domian.domian.Session
import space.littleinferno.schedule.server.domain
import space.littleinferno.schedule.server.domain.Teacher
import space.littleinferno.schedule.server.repositories.TeacherRepository

object TeacherService {
  type Service[F[_]] = SimpleRestService[F, Int, Teacher]
}

class TeacherServiceImpl[F[_]: Sync](repo: TeacherRepository[F]) extends TeacherService.Service[F] {
  def list: F[List[domain.Teacher]] = repo.list

  def create(session: Session, body: Teacher): F[Teacher] = repo.create(body)

  def edit(session: Session, id: Int, body: Teacher): F[Teacher] =
    repo.edit(id, body)

  def delete(session: Session, id: Int): F[Unit] = repo.delete(id)
}
