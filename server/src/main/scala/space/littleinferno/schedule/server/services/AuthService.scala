package space.littleinferno.schedule.server.services

import cats.ApplicativeError
import cats.effect.{IO, Sync}
import cats.instances.option._
import cats.syntax.functor._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.traverse._
import space.littleinferno.schedule.domian.domian.{Credentials, Session, Token, TokenPair}
import space.littleinferno.schedule.domian.instances._
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.RegisterInfo
import space.littleinferno.schedule.server.repositories.UserRepository

trait AuthService[F[_]] {
  def registerToken(session: Session): F[Token]

  def register(registerInfo: RegisterInfo): F[TokenPair]

  def login(credentials: Credentials): F[TokenPair]
}

class AuthServiceImpl[F[_]: Sync](tokenProvider: TokenProvider, userRepository: UserRepository[F])(
    implicit F: ApplicativeError[F, Throwable]
) extends AuthService[F] {
  def registerToken(session: Session): F[Token] =
    Token(tokenProvider.register(session.login)).pure[F]

  def register(registerInfo: RegisterInfo): F[TokenPair] =
    tokenProvider
      .validateRegister(registerInfo.token)
      .traverse(
        _ =>
          userRepository
            .create(registerInfo.credentials)
            .map(
              _ =>
                TokenPair(
                  tokenProvider.access(registerInfo.credentials.login),
                  tokenProvider.refresh(registerInfo.credentials.login)
              )
          )
      )
      .flatMap(tok => tok.fold(F.raiseError(new Throwable("invalid token")): F[TokenPair])(_.pure[F]))

  def login(credentials: Credentials): F[TokenPair] =
    userRepository
      .checkCredentials(credentials)
      .ifM(
        TokenPair(tokenProvider.access(credentials.login), tokenProvider.refresh(credentials.login))
          .pure[F],
        F.raiseError(new Throwable("invalid credentials"))
      )
}
