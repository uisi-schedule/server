package space.littleinferno.schedule.server.modules

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ru.tinkoff.tschema.swagger.{MkSwagger, SwaggerBuilder}

trait Module {
  def route: Route            = reject
  def swagger: SwaggerBuilder = MkSwagger.empty
}
