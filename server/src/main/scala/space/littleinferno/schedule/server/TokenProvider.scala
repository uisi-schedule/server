package space.littleinferno.schedule.server

import java.time.Instant
import java.time.temporal.ChronoUnit

import scala.concurrent.ExecutionContext.Implicits.global

import black.door.jose.json.circe.JsonSupport._
import black.door.jose.jwk.P256KeyPair
import black.door.jose.jws.KeyResolver._
import black.door.jose.jwt.{Check, Claims, Jwt, JwtValidator}
import cats.syntax.option._

import space.littleinferno.schedule.domian.domian.Token
import space.littleinferno.schedule.domian.instances._
import space.littleinferno.schedule.domian.predef.Login
import space.littleinferno.schedule.server.config.config.TokenConfig

class TokenProvider(config: TokenConfig) {
  def access(login: Login): String =
    encode(Claims(sub = login.repr.some, iss = config.issuer.some, exp = TokenProvider.expAccess))

  def refresh(login: Login): String =
    encode(Claims(sub = login.repr.some, iss = config.issuer.some, exp = TokenProvider.expRefresh))

  def register(login: Login): String =
    encode(Claims(sub = login.repr.some, aud = config.issuer.some, exp = TokenProvider.expRegister))

  def validateAuth(token: String): Option[Login] =
    validate(token, Check.iss(_ == config.issuer, required = true))

  def validateRegister(token: String): Option[Login] =
    validate(token, Check.aud(_ == config.issuer, required = true))

  private def encode(claims: Claims[Unit]): String =
    Jwt.sign(claims, TokenProvider.key)

  private def validate(token: String, validator: JwtValidator[Unit]): Option[Login] =
    Jwt
      .validate(token)
      .using(TokenProvider.key, validator)
      .now
      .toOption
      .flatMap(_.claims.sub.map(_.coerce[Login]))
}

object TokenProvider {
  private val key: P256KeyPair = P256KeyPair.generate.copy(alg = "ES256".some)
  private def expRegister      = Instant.now().plus(10, ChronoUnit.MINUTES).some
  private def expAccess        = Instant.now().plus(24, ChronoUnit.HOURS).some
  private def expRefresh       = Instant.now().plus(28, ChronoUnit.DAYS).some
}
