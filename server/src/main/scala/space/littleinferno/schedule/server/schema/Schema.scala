package space.littleinferno.schedule.server.schema

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive0, Route}
import akka.stream.ActorMaterializer
import cats.effect.{ContextShift, IO}
import cats.syntax.applicative._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import ru.tinkoff.tschema.swagger.{MkSwagger, OpenApiInfo}
import com.typesafe.config.{Config, ConfigFactory}
import ru.tinkoff.tschema.swagger.PathDescription.{MethodTarget, Target, TypeTarget}
import ru.tinkoff.tschema.swagger.{OpenApi, OpenApiInfo, PathDescription, SwaggerDescription}
import space.littleinferno.schedule.server.modules.{Module, ScheduleModule}
import space.littleinferno.schedule.server.schema.Schema.{Templates, TypedEntry}
import cats.implicits._

final class Schema(modules: List[Module]) {
  lazy val descriptions: PathDescription.DescriptionMap = Templates(ConfigFactory.load("swagger.conf"))

  def api: TypedEntry      = TypedEntry(modules, descriptions)
}

object Schema {
  trait TypedEntry {
    def route: Route
    def openApi: OpenApi
  }

  object TypedEntry {
    def apply(modules: List[Module], descriptions: => Target => Option[SwaggerDescription]): TypedEntry =
      new TypedEntry {
        def route: Route = modules.map(_.route).fold(reject)(_ ~ _)
        lazy val openApi: OpenApi = modules
          .map(_.swagger)
          .fold(MkSwagger.empty)(_ ++ _)
          .describe(descriptions)
          .make(OpenApiInfo())
          .addServer("/")
      }
  }

  object Templates {
    def apply(cfg: Config): PathDescription.DescriptionMap = {
      def readKey(name: String) =
        if (cfg.hasPath(name)) cfg.getString(name).some else None

      {
        case Target.Tag(key) => readKey(s"$key.tag")
        case Target.Method(key, sub) =>
          sub match {
            case MethodTarget.Path        => readKey(s"$key.path")
            case MethodTarget.Body        => readKey(s"$key.body")
            case MethodTarget.Summary     => readKey(s"$key.summary")
            case MethodTarget.Param(name) => readKey(s"$key.$name")
          }
        case Target.Type(name, sub) =>
          sub match {
            case TypeTarget.Type         => readKey(s"types.$name.info")
            case TypeTarget.Title        => readKey(s"types.$name.title")
            case TypeTarget.Field(field) => readKey(s"types.$name.$field")
          }
      }
    }
  }
}
