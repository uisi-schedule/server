package space.littleinferno.schedule.server

import java.time.LocalDate

import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.{httpParam, openapiParam, swagger}

import space.littleinferno.schedule.domian.domian.{Credentials, Token}
import space.littleinferno.schedule.domian.predef._

object domain {
  implicit val codec: Configuration = Configuration.default.withDiscriminator("type")

  @derive(decoder, swagger)
  case class RegisterInfo(token: String, credentials: Credentials)

  @derive(encoder, decoder, swagger)
  final case class ServerInfo(status: Boolean)

  @ConfiguredJsonCodec
  @derive(swagger)
  sealed trait LessonGetFormat {
    def timeSlot: TimeSlot
  }

  case class SingleLessonByGroupFormat(
      name: String,
      lessonType: LessonType,
      subGroup: Option[Int],
      teacher: String,
      auditorium: Auditorium,
      timeSlot: TimeSlot
  ) extends LessonGetFormat

  case class MultipleLessonByGroupFormat(timeSlot: TimeSlot, lessons: List[LessonGetFormat]) extends LessonGetFormat

  case class LessonByTeacherFormat(
      groups: List[String],
      name: String,
      lessonType: LessonType,
      subGroup: Option[Int],
      auditorium: Auditorium,
      timeSlot: TimeSlot
  ) extends LessonGetFormat

  @derive(encoder, decoder, swagger)
  case class Day(date: LocalDate, lessons: List[LessonGetFormat])

  sealed trait ScheduleGetFormat

  @derive(encoder, decoder, swagger)
  case class ScheduleByGroupFormat(group: String, days: List[Day]) extends ScheduleGetFormat

  @derive(encoder, decoder, swagger)
  case class ScheduleByTeacherFormat(teacher: String, days: List[Day]) extends ScheduleGetFormat

  @derive(encoder, decoder, swagger)
  case class ScheduleShortInfo(id: Int, group: String, kind: ScheduleKind, begin: Option[LocalDate], end: Option[LocalDate])

  sealed trait GraduateLevel extends EnumEntry

  object GraduateLevel extends Enum[GraduateLevel] {
    def values: IndexedSeq[GraduateLevel] = findValues

    case object College  extends GraduateLevel
    case object Bachelor extends GraduateLevel
    case object Master   extends GraduateLevel
  }

  @derive(encoder, decoder, swagger)
  case class Group(name: String, course: Int, graduateLevel: GraduateLevel, id: Option[Int])

  @derive(encoder, decoder, swagger)
  case class Lesson(name: String, id: Option[Int])

  @derive(encoder, decoder, swagger)
  case class Teacher(name: String, id: Option[Int])
}
