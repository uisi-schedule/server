package space.littleinferno.schedule.server.repositories

import cats.effect.Sync
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.traverse._
import doobie.ConnectionIO
import doobie.free.connection
import doobie.implicits._
import space.littleinferno.schedule.server.repositories.implicits._
import doobie.util.transactor.Transactor
import space.littleinferno.schedule.server.domain.Group

trait GroupRepository[F[_]] {
  def list: F[List[Group]]

  def create(group: Group): F[Group]

  def edit(id: Int, group: Group): F[Group]

  def delete(id: Int): F[Unit]
}

class GroupRepositoryImpl[F[_]: Sync](implicit xa: Transactor.Aux[F, Unit]) extends GroupRepository[F] {
  def list: F[List[Group]] =
    sql"select name, course, graduate_level, id from groups"
      .query[Group]
      .to[List]
      .transact(xa)

  def create(group: Group): F[Group] =
    sql"""insert into groups (name, course, graduate_level)
          values (${group.name},${group.course}, ${group.graduateLevel})
          returning name, course, graduate_level, id"""
      .query[Group]
      .unique
      .transact(xa)

  def edit(id: Int, group: Group): F[Group] =
    sql"""update groups set name = ${group.name}, course = ${group.course}, graduate_level = ${group.graduateLevel}
          where id = $id
          returning name, course, graduate_level, id"""
      .query[Group]
      .unique
      .transact(xa)

  def delete(id: Int): F[Unit] =
    sql"delete from groups where id = $id".update.run
      .transact(xa)
      .void
}
