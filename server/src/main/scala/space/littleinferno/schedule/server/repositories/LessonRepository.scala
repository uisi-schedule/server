package space.littleinferno.schedule.server.repositories

import cats.effect.Sync
import cats.instances.list._
import cats.instances.option._
import cats.syntax.applicative._
import cats.syntax.functor._
import cats.syntax.traverse._
import doobie.implicits._
import doobie.util.transactor.Transactor
import space.littleinferno.schedule.server.domain.Lesson

trait LessonRepository[F[_]] {
  def list: F[List[Lesson]]

  def create(lesson: Lesson): F[Lesson]

  def edit(id: Int, lesson: Lesson): F[Lesson]

  def delete(id: Int): F[Unit]
}

class LessonRepositoryImpl[F[_]: Sync](implicit xa: Transactor.Aux[F, Unit]) extends LessonRepository[F] {
  def list: F[List[Lesson]] =
    sql"select name, id from lessons"
      .query[Lesson]
      .to[List]
      .transact(xa)

  def create(lesson: Lesson): F[Lesson] =
    sql"""insert into lessons (name)
          values (${lesson.name})
          returning name, id"""
      .query[Lesson]
      .unique
      .transact(xa)

  def edit(id: Int, lesson: Lesson): F[Lesson] =
    sql"""update lessons set name = ${lesson.name}
          where id = $id
          returning name, id"""
      .query[Lesson]
      .unique
      .transact(xa)

  def delete(id: Int): F[Unit] =
    sql"delete from lessons where id = $id".update.run
      .transact(xa)
      .void
}
