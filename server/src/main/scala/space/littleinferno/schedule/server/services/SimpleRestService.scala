package space.littleinferno.schedule.server.services

import space.littleinferno.schedule.domian.domian.Session

trait SimpleRestService[F[_], Id, Object] {
  def list: F[List[Object]]

  def create(session: Session, body: Object): F[Object]

  def edit(session: Session, id: Id, body: Object): F[Object]

  def delete(session: Session, id: Id): F[Unit]
}
