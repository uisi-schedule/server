package space.littleinferno.schedule.server.exporter

import space.littleinferno.schedule.domian.predef.ScheduleFormat
import space.littleinferno.schedule.server.domain.ScheduleGetFormat
import space.littleinferno.schedule.server.tschema.instances.Excel

trait Exporter[F[_]] {
  def export(ss: ScheduleFormat): F[Excel]
}
