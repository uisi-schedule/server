package space.littleinferno.schedule.server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directive0, Route}
import akka.stream.ActorMaterializer
import cats.effect.{ContextShift, IO}
import cats.syntax.applicative._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.Printer
import space.littleinferno.schedule.server.config.config.ServerConfig
import space.littleinferno.schedule.server.schema.Schema
import space.littleinferno.schedule.server.tschema.instances.CatsIO

class Server(config: ServerConfig, schema: Schema)(implicit system: ActorSystem, materializer: ActorMaterializer)
  extends CatsIO with FailFastCirceSupport with CorsSupport {

  def run(implicit cs: ContextShift[IO]): IO[ServerBinding] = {
    val allRoutes = corsHandler(schema.api.route ~ statics)
    IO.fromFuture(akka.http.scaladsl.Http().bindAndHandle(allRoutes, config.host, config.port).pure[IO])
  }

  private[this] implicit val printer: Printer =
    Printer.noSpaces.copy(dropNullValues = true)

  private val statics =
  path("schema" / "swagger")(complete(schema.api.openApi)) ~
  pathPrefix("webjars")(get(getFromResourceDirectory("META-INF/resources/webjars"))) ~
  path("swagger")(getFromResource("static/swagger.html")) ~
  pathEndOrSingleSlash(getFromResource("static/index.html"))

}
trait CorsSupport {
  private def addAccessControlHeaders: Directive0 =
    respondWithHeaders(
      `Access-Control-Allow-Origin`(HttpOriginRange.*),
      `Access-Control-Allow-Headers`(
        "content-type",
        "access-control-allow-headers",
        "access-control-allow-origin",
        "authorization"
      )
    )

  private def preflightRequestHandler: Route = options {
    complete(HttpResponse(StatusCodes.OK).withHeaders(`Access-Control-Allow-Methods`(OPTIONS, PUT, POST, GET, DELETE)))
  }

  def corsHandler(r: Route) = addAccessControlHeaders {
    preflightRequestHandler ~ r
  }
}
