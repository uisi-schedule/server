package space.littleinferno.schedule.server.config

import java.io.File

import cats.effect.IO
import com.typesafe.config.{ConfigFactory, ConfigResolveOptions}
import io.circe.config.parser
import org.manatki.derevo.circeDerivation.decoder
import org.manatki.derevo.derive
import space.littleinferno.schedule.domian.domian.Credentials

object config {

  @derive(decoder)
  case class DatabaseConfig(url: String, user: String, password: String)

  @derive(decoder)
  case class ServerConfig(host: String, port: Int)

  @derive(decoder)
  case class TokenConfig(issuer: String)

  @derive(decoder)
  case class AppConfig(database: DatabaseConfig,
                       server: ServerConfig,
                       token: TokenConfig,
                       bootstrap: Option[Credentials])

  object AppConfig {
    def load(): IO[AppConfig] = {
      val origin = ConfigFactory.load("application.conf")
      val conf = sys.env.get("override").fold(origin) { p =>
        origin.withFallback(ConfigFactory.parseFile(new File(p)))
      }
      parser.decodeF[IO, AppConfig](conf)
    }
  }

}
