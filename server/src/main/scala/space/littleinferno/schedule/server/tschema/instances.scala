package space.littleinferno.schedule.server.tschema

import java.time.LocalDate

import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller, ToResponseMarshaller}
import akka.http.scaladsl.model.{ContentType, HttpEntity, MediaType}
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.Credentials
import akka.util.ByteString
import cats.effect.IO
import cats.syntax.option._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.extras.Configuration
import io.estatico.newtype.NewType
import ru.tinkoff.tschema.akkaHttp.{Routable, Serve}
import ru.tinkoff.tschema.akkaHttp.auth.{BasicAuthenticator, BearerAuthenticator}
import ru.tinkoff.tschema.common.Name
import ru.tinkoff.tschema.param.{HttpParam, HttpSingleParam}
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.swagger.SwaggerMapper._
import ru.tinkoff.tschema.typeDSL.{ApiKeyAuth, CanHoldApiKey, DSLAtom}
import ru.tinkoff.tschema.syntax._
import ru.tinkoff.tschema.typeDSL
import shapeless.{HList, Witness}
import space.littleinferno.schedule.domian.domian.Session
import space.littleinferno.schedule.server.TokenProvider

object instances extends FailFastCirceSupport {

  //noinspection TypeAnnotation
  def session = bearerAuth[Session]("Schedule session", Symbol("session"))

  implicit def authenticator(implicit tokenProvider: TokenProvider): BearerAuthenticator[Session] =
    BearerAuthenticator.of {
      case Credentials.Missing         => None
      case Credentials.Provided(token) => tokenProvider.validateAuth(token).map(Session.apply)
    }

  implicit val dateHttpParam: HttpParam[LocalDate] = HttpSingleParam.stringParam.map(LocalDate.parse)

//  private implicit val config: Configuration =
//    Configuration.default.withDiscriminator("status")
//
//  sealed trait Res[A]
//
//  final case class SuccessValue[A](payload: A) extends Res[A]
//
//  final case class Fail[A](payload: A) extends Res[A]
//
//  final case class Error[A](message: String) extends Res[A]
//
//  type ResIO[A] = IO[Res[A]]
//
//  object ResIO {
//    def unit: ResIO[Unit] = IO.pure(SuccessValue())
//  }

  trait CatsIO {
    implicit def ioMarshaller[A, B](implicit m: Marshaller[A, B]): Marshaller[IO[A], B] =
      Marshaller { implicit ec => a =>
        a.unsafeToFuture.flatMap(m(_))
      }
  }

  implicit def routable[A: ToResponseMarshaller]: Routable[IO[A], A] =
    new Routable[IO[A], A] {
      import akka.http.scaladsl.model._
      import akka.http.scaladsl.server.Directives.onComplete

      import scala.util.{Failure, Success}

      override def route(res: => IO[A]): Route =
        onComplete(res.unsafeToFuture) {
          case Success(success) =>
            complete(success)
          case Failure(e) =>
            val exceptionMessage = s"${e.toString} ${e.getStackTrace.mkString("\n")}"
            complete(HttpResponse(StatusCodes.InternalServerError, entity = exceptionMessage))
        }
    }

  type Excel = Excel.Type
  object Excel extends NewType.Default[Array[Byte]]

  implicit def resSwagger: SwaggerTypeable[Excel] =
    new SwaggerTypeable[Excel] {
      override def typ: SwaggerType =
        SwaggerPrimitive.bin("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    }

  private val media =
    MediaType
      .applicationBinary("vnd.openxmlformats-officedocument.spreadsheetml.sheet", MediaType.NotCompressible, ".xlsx")
  implicit def ArrayMarshaller: ToEntityMarshaller[Excel] =
    Marshaller.withFixedContentType(media) { array =>
      HttpEntity(media, array.repr)
    }
}
