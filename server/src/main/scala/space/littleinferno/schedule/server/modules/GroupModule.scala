package space.littleinferno.schedule.server.modules

import cats.effect.IO
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.Group
import space.littleinferno.schedule.server.services.GroupService.Service
import space.littleinferno.schedule.server.tschema.instances._

class GroupModule(service: Service[IO])(implicit tokenProvider: TokenProvider) extends Module {
  import GroupModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object GroupModule {
  private def api =
    tagPrefix("groups") |>
    (groupList <> createGroup <> editGroup <> deleteGroup)

  private def groupList =
    get |>
    key("list") |>
    $$[List[Group]]

  private def createGroup =
    post |>
    session |>
    key("create") |>
    reqBody[Group] |>
    $$[Group]

  private def editGroup =
    put |>
    session |>
    key("edit") |>
    capture[Int]("id") |>
    reqBody[Group] |>
    $$[Group]

  private def deleteGroup =
    delete |>
    session |>
    key("delete") |>
    capture[Int]("id") |>
    $$[Unit]

}
