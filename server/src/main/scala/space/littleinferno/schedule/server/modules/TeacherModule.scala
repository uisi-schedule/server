package space.littleinferno.schedule.server.modules

import cats.effect.IO
import ru.tinkoff.tschema.akkaHttp.MkRoute
import ru.tinkoff.tschema.swagger._
import ru.tinkoff.tschema.syntax._
import space.littleinferno.schedule.server.TokenProvider
import space.littleinferno.schedule.server.domain.Teacher
import space.littleinferno.schedule.server.services.TeacherService.Service
import space.littleinferno.schedule.server.tschema.instances._

class TeacherModule(service: Service[IO])(implicit tokenProvider: TokenProvider) extends Module {
  import TeacherModule._

  override val route = MkRoute(api)(service)

  override val swagger = api.mkSwagger
}

object TeacherModule {
  private def api =
    tagPrefix("teachers") |>
    (teacherList <> createTeacher <> editTeacher <> deleteTeacher)

  private def teacherList =
    get |>
    key("list") |>
    $$[List[Teacher]]

  private def createTeacher =
    post |>
    session |>
    key("create") |>
    reqBody[Teacher] |>
    $$[Teacher]

  private def editTeacher =
    put |>
    session |>
    key("edit") |>
    capture[Int]("id") |>
    reqBody[Teacher] |>
    $$[Teacher]

  private def deleteTeacher =
    delete |>
    session |>
    key("delete") |>
    capture[Int]("id") |>
    $$[Unit]

}
