package space.littleinferno.schedule.server.repositories

import cats.effect.{Async, ContextShift}
import doobie.util.transactor
import doobie.util.transactor.Transactor.Aux

import space.littleinferno.schedule.server.config.config.DatabaseConfig

class Repositories[F[_]: Async](config: DatabaseConfig)(implicit context: ContextShift[F]) {
  implicit val xa: Aux[F, Unit] =
    transactor.Transactor.fromDriverManager[F]("org.postgresql.Driver", config.url, config.user, config.password)

  val scheduleRepository: ScheduleRepository[F] = new ScheduleRepository[F]
  val userRepository: UserRepository[F]         = new UserRepository[F]
  val groupRepository: GroupRepository[F]       = new GroupRepositoryImpl[F]
  val lessonRepository: LessonRepository[F]     = new LessonRepositoryImpl[F]
  val teacherRepository: TeacherRepository[F]   = new TeacherRepositoryImpl[F]
}
