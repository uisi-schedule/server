alter table lesson_list
    rename to lessons;

alter table teacher_list
    rename to teachers;

alter table group_list
    rename to groups;

alter table lesson
    rename to schedule_lessons;
