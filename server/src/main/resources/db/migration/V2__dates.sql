create table dates
(
    lesson_id integer not null
        constraint table_name_lesson_id_fk
            references lesson (id),
    date      date
);