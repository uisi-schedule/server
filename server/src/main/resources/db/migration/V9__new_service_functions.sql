create or replace function publish_schedule(_schedule_id integer) returns void
    language plpgsql
as
$$
declare
    _schedule schedule%rowtype;
begin
    select *
    from schedule
    where id = _schedule_id
    into _schedule;

    if _schedule.kind = 'Archive' then
        raise exception 'Invalid schedule kind: Archive';
    end if;

    update schedule
    set kind = 'Archive'
    where group_name = _schedule.group_name
      and kind = 'Actual';

    update schedule
    set kind = 'Actual'
    where id = _schedule.id;
end;
$$;

alter table schedule_lessons
    add constraint schedule_lessons_schedule_id_fk
        foreign key (schedule_id) references schedule
            on delete cascade;


