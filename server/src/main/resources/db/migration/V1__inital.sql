create table users
(
    login    varchar not null
        constraint users_pk
            primary key,
    password varchar not null
);

create unique index users_login_uindex
    on users (login);

create table teachers
(
    name varchar not null
);

create unique index teachers_name_uindex
    on teachers (name);

create table groups
(
    name varchar not null
);

create unique index groups_name_uindex
    on groups (name);

create table schedule
(
    id         serial not null
        constraint schedule_pk
            primary key,
    group_name varchar,
    begin_date date,
    end_date   date
);

create unique index schedule_id_uindex
    on schedule (id);

create type lesson_type as enum ('Lecture', 'Practice', 'Laboratory', 'Test', 'Exam', 'Action');

create table lesson
(
    id          serial      not null,
    schedule_id integer     not null,
    number      integer     not null,
    name        varchar     not null,
    lesson_type lesson_type not null,
    sub_group   integer,
    teacher     varchar     not null,
    classroom   varchar
);

create unique index lesson_id_uindex
    on lesson (id);