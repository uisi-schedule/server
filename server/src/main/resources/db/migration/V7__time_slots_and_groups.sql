create table time_slots
(
    id serial not null
        constraint time_slots_pk
            primary key,
    slot_begin time not null,
    slot_end time not null,
    number int
);

create unique index time_slots_uindex
    on time_slots (id, slot_begin, slot_end, number);

alter table schedule_lessons
    add time_slot_id int;

alter table schedule_lessons drop constraint lesson_unique;

alter table schedule_lessons drop column number;

create unique index lesson_unique
    on schedule_lessons (schedule_id, name, lesson_type, teacher, auditorium, building, dates, time_slot_id);

alter table schedule_lessons
    add constraint schedule_lessons_time_slots_id_fk
        foreign key (time_slot_id) references time_slots
            on update cascade on delete restrict;

alter table groups
    add course int;