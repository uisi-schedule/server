alter table teachers
    add id serial;

alter table lessons
    add id serial;

create type graduate_level as enum ('College', 'Bachelor', 'Master');

alter table groups
    add graduate_level graduate_level,
    add id             serial;