alter table schedule
    add constraint schedule_groups_name_fk
        foreign key (group_name) references groups (name)
            on update cascade on delete cascade;

alter table schedule_lessons
    add constraint schedule_lessons_lessons_name_fk
        foreign key (name) references lessons (name)
            on update cascade on delete restrict;

alter table schedule_lessons
    add constraint schedule_lessons_teachers_name_fk
        foreign key (teacher) references teachers (name)
            on update cascade on delete restrict;

create or replace function add_schedule_draft(_group_name varchar) returns integer
as
$$
declare
    _id integer;
begin
    select id from schedule where group_name = _group_name and kind = 'Draft' into _id;

    if _id is not null then
        delete from schedule_lessons where schedule_id = _id;
    else
        insert into schedule (group_name, kind)
        values (_group_name, 'Draft')
        returning id into _id;
    end if;

    return _id;
end;
$$ language plpgsql;

create or replace function publish_schedule(_group_name varchar) returns void
as
$$
begin
    update schedule set kind = 'Archive' where group_name = _group_name and kind = 'Actual';

    update schedule
    set kind = 'Actual'
    where id = (select id from schedule where group_name = _group_name and kind = 'Draft');
end;
$$ language plpgsql;


create or replace function update_schedule_border(_schedule_id integer, _dates date[]) returns void
as
$$
declare
    _old_borders record;
    _new_borders record;
begin
    select start_date, end_date
    from schedule
    where id = _schedule_id
    into _old_borders;

    select min(date), max(date)
    from unnest(_dates) as date
    into _new_borders;

    if _old_borders.start_date is null or _new_borders.min < _old_borders.start_date then
        update schedule set start_date = _new_borders.min where id = _schedule_id;
    end if;

    if _old_borders.end_date is null or _new_borders.max > _old_borders.end_date then
        update schedule set end_date = _new_borders.max where id = _schedule_id;
    end if;
end;
$$ language plpgsql;

create or replace function add_lesson(_schedule_id integer, _number integer, _name varchar, _lesson_type lesson_type,
                                      _sub_group integer, _teacher varchar, _dates date[], _auditorium varchar,
                                      _building integer) returns void
as
$$
begin
    insert into schedule_lessons(schedule_id, number, name, lesson_type, sub_group, teacher, dates, auditorium, building)
    values (_schedule_id, _number, _name, _lesson_type,
            _sub_group, _teacher, _dates, _auditorium,
            _building)
    on conflict do nothing;
end;
$$ language plpgsql;

