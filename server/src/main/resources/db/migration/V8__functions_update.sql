drop function add_lesson(_schedule_id integer, _number integer, _name varchar, _lesson_type lesson_type,
    _sub_group integer, _teacher varchar, _dates date[], _auditorium varchar,
    _building integer);

create function add_lesson(_schedule_id integer, _name character varying, _lesson_type lesson_type, _sub_group integer,
                           _teacher character varying, _dates date[], _auditorium character varying, _building integer,
                           _time_slot_id integer) returns void
    language plpgsql
as
$$
begin
    insert into schedule_lessons(schedule_id, name, lesson_type, sub_group, teacher, dates, auditorium, building,
                                 time_slot_id)
    values (_schedule_id, _name, _lesson_type,
            _sub_group, _teacher, _dates, _auditorium,
            _building, _time_slot_id)
    on conflict do nothing;

    perform update_schedule_border(_schedule_id, _dates);
end;
$$;

create or replace function day_by_teacher(_teacher varchar, _day date)
    returns table
            (
                groups      varchar[],
                name        varchar,
                lesson_type lesson_type,
                sub_group   integer,
                auditorium  varchar,
                building    integer,
                slot_begin  time,
                slot_end    time,
                number      integer
            )
as
$$
begin
    return query select array_agg(schedule.group_name) as groups,
                        schedule_lessons.name,
                        schedule_lessons.lesson_type,
                        schedule_lessons.sub_group,
                        schedule_lessons.auditorium,
                        schedule_lessons.building,
                        slot.slot_begin,
                        slot.slot_end,
                        slot.number
                 from schedule_lessons
                          left join (select max(id), group_name
                                     from schedule
                                     where start_date <= _day
                                       and end_date >= _day
                                     group by group_name) as schedule
                                    on schedule_lessons.schedule_id = schedule.max
                          left join (select time_slots.id,
                                            time_slots.slot_begin,
                                            time_slots.slot_end,
                                            time_slots.number
                                     from time_slots) as slot
                                    on schedule_lessons.time_slot_id = slot.id
                 where group_name is not null
                   and teacher = _teacher
                   and _day = any (schedule_lessons.dates)
                 group by schedule_lessons.name,
                          schedule_lessons.lesson_type,
                          schedule_lessons.sub_group,
                          schedule_lessons.auditorium,
                          schedule_lessons.building,
                          slot.slot_begin,
                          slot.slot_end,
                          slot.number;
end;
$$ language plpgsql;

create or replace function day_by_group(_group varchar, _date date)
    returns table
            (
                name        character varying,
                lesson_type lesson_type,
                sub_group   integer,
                teacher     character varying,
                auditorium  character varying,
                building    integer,
                slot_begin  time,
                slot_end    time,
                number      integer
            )
    language plpgsql
as
$$
begin
    return query select schedule_lessons.name,
                        schedule_lessons.lesson_type,
                        schedule_lessons.sub_group,
                        schedule_lessons.teacher,
                        schedule_lessons.auditorium,
                        schedule_lessons.building,
                        slot.slot_begin,
                        slot.slot_end,
                        slot.number
                 from schedule_lessons
                          left join (select time_slots.id,
                                            time_slots.slot_begin,
                                            time_slots.slot_end,
                                            time_slots.number
                                     from time_slots) as slot
                                    on schedule_lessons.time_slot_id = slot.id
                 where schedule_lessons.schedule_id = (
                     select id
                     from schedule
                     where group_name = _group
                       and start_date <= _date
                       and end_date >= _date
                     order by id desc
                     limit 1
                 )
                   and _date = any (schedule_lessons.dates);
end;
$$;

