create table lesson_list
(
    name varchar not null
);

alter table teachers
    rename to teacher_list;

alter table groups
    rename to group_list;

drop table dates;

drop index lesson_id_uindex;

alter table lesson
    add column auditorium varchar,
    add column building integer,
    drop column id,
    drop column classroom,
    add column dates date[],
    add constraint lesson_unique
        unique (schedule_id, number, name, lesson_type, teacher, auditorium, building, dates);

create type schedule_kind as enum ('Actual', 'Archive', 'Draft');

alter table schedule
    rename begin_date to start_date;

alter table schedule
add column kind schedule_kind;
