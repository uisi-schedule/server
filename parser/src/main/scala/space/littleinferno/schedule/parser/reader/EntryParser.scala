package space.littleinferno.schedule.parser.reader

import cats.syntax.option._
import org.parboiled2.{CharPredicate, Parser, ParserInput}
import zio.ZIO
import space.littleinferno.schedule.parser.types.ScheduleFF
import space.littleinferno.schedule.parser.reader.utils._

class EntryParser(val input: ParserInput) extends Parser {
  import EntryParser._

  private def ws     = rule(whitespaces.*)
  private def word   = rule(capture(startAlpha ~ alpha.+))
  private def digits = rule(capture(digit.+) ~> (_.toInt))

  private def semicolon = rule(ws ~ CharPredicate(";").+ ~ ws)
  private def dash      = rule(ws ~ CharPredicate("-").+ ~ ws)
  private def colon     = rule(ws ~ CharPredicate(":").+ ~ ws)
  private def comma     = rule(ws ~ CharPredicate(",").+ ~ ws)

  private def delimeter  = rule(semicolon | colon)
  private def delimeter2 = rule(semicolon | dash)
  private def delimeter3 = rule(colon | dash)
  private def delimeter5 = rule(semicolon | comma)

  private def day       = rule(digits ~> Day)
  private def month     = rule(day.+(",") ~ "." ~ digits ~> Month)
  private def year      = rule(month.+(semicolon) ~ "." ~ digits ~ "г" ~ ".".? ~> Date)
  private def dateRange = rule("с" ~ ws ~ &(digit) ~ year ~ ws ~ "по" ~ ws ~ year ~> DatePeriod)
  private def date      = rule((year | dateRange).+(semicolon) ~> DateList)

  private def lessonType =
    rule(
      ((capture(alpha2.+) ~ &((dash | ws) ~ subGroup)) | word) ~> (
          lessonType => test(isLessonType(lessonType)) ~ push(extractLessonType(lessonType))
      )
    )
  private def subGroup = rule(digits ~ ws.? ~ "п/гр" ~ CharPredicate(".").*)

  private def dateSubGroup    = rule(date ~ dash ~ subGroup ~> DateSubGroup)
  private def subGroupTeacher = rule(subGroup ~ dash ~ word ~> SubGroupTeacher)
  private def dateType =
    rule(date ~ dash ~ lessonType ~ ((dash | ws) ~ subGroup).? ~> ((a, b, c) => DateType(a, b, c)))

  private def lessonTypeDateSubGroupTeacher =
    rule(lessonType ~ delimeter ~ dateSubGroup.+(delimeter) ~ delimeter.? ~ ws ~ word ~> {
      (lessonType, dateSubGroups, teacher) =>
        dateSubGroups.map(dsg => DateTypesSubGroupTeacher(dsg.dates, lessonType, dsg.subGroup.some, teacher))
    })

  private def dateSubGroupTeacher3 =
    rule(date ~ dash ~ lessonType ~ colon ~ subGroupTeacher.+(semicolon) ~> { (dates, lessonType, subGroupTeachers) =>
      subGroupTeachers.map(sgt => DateTypesSubGroupTeacher(dates, lessonType, sgt.subGroup.some, sgt.teacher))
    })

  private def dateSubGroupTeacher2 =
    rule(date ~ dash.? ~ subGroup ~ delimeter ~ ws ~ word ~> ((a, b, c) => Seq(DateSubGroupTeacher(a, b, c))))

  private def dateTypesSubGroupTeacher =
    rule(dateType.+(delimeter2) ~ colon ~ word ~> { (dateTypes, teacher) =>
      dateTypes.map(dt => DateTypesSubGroupTeacher(dt.dates, dt.lessonType, dt.subGroup, teacher))
    })

  private def withOutName =
    rule(
      ((&(lessonType) ~ lessonTypeDateSubGroupTeacher) | (dateSubGroupTeacher3 | dateSubGroupTeacher2 | dateTypesSubGroupTeacher))
        .+(semicolon) ~> (_.flatten.toList match {
        case (head: DateTypesSubGroupTeacher) :: Nil => push(Seq(head))
        case (head: DateTypesSubGroupTeacher) :: tail =>
          push(
            tail.foldLeft(Seq(head))(
              (acc, i) =>
                i match {
                  case withType: DateTypesSubGroupTeacher => acc :+ withType
                  case withoutType: DateSubGroupTeacher =>
                    acc :+ DateTypesSubGroupTeacher(
                      withoutType.dates,
                      acc.last.lessonType,
                      withoutType.subGroup.some,
                      withoutType.teacher
                    )
              }
            )
          )
        case _ => push(Seq.empty[DateTypesSubGroupTeacher]) ~ fail("type of lessson canot find")
      })
    )

  private def section221(n: Int) =
    rule(ws ~ word ~ delimeter ~ withOutName ~> { (name, dateSubGroupTeacher) =>
      dateSubGroupTeacher
        .map(dt => Lesson(n, name, dt.dates.localDates, dt.lessonType, dt.subGroup, extractTeacherName(dt.teacher)))
    })

  private def section3(n: Int) =
    rule(ws ~ date ~ ws ~ word ~ semicolon ~ lessonType ~ (delimeter3.? ~ subGroup).? ~ colon ~ word ~> {
      (dates, name, lessonType, subGroup, tName) =>
        List(Lesson(n, name, dates.localDates, lessonType, subGroup, extractTeacherName(tName)))
    })

  def all(n: Int) =
    rule(((!(date) ~ section221(n)) | section3(n)).+(semicolon) ~ semicolon.? ~ ws.? ~ EOI ~> (_.flatten))

  def classrooms = rule(digits.+(comma) ~ ws.? ~ "УК№" ~ digits ~> RawAuditorium)

  def rims = rule(ws ~ capture(rim.+) ~ ws ~> (a=>  RomanAuditorium(a)))

  def stubRooms = rule(capture(alpha3.+).+(comma) ~> StubAuditorium)

  def _rooms = rule(ws.? ~ (classrooms | rims | stubRooms).+(delimeter5) ~ delimeter5.? ~ ws.? ~ EOI)

  def rooms = rule(_rooms ~> (_.flatMap(_.rooms)))
}

private object EntryParser {
  def apply(input: ParserInput): ScheduleFF[EntryParser] = ZIO.succeed(new EntryParser(input))

  val digit: CharPredicate = CharPredicate.Digit

  val lowerAlpha: CharPredicate = CharPredicate('а' to 'я')
  val upperAlpha: CharPredicate = CharPredicate('А' to 'Я')

  val startAlpha: CharPredicate = lowerAlpha ++ upperAlpha
  val alpha: CharPredicate      = lowerAlpha ++ upperAlpha ++ digit ++ " -.,()"

  val alpha2: CharPredicate = lowerAlpha ++ upperAlpha ++ "."

  val rim: CharPredicate    = CharPredicate("VI")
  val alpha3: CharPredicate = lowerAlpha ++ upperAlpha ++ " ./"

  val whitespaces: CharPredicate = CharPredicate(" \n\t\n")
}
