package space.littleinferno.schedule.parser.reader

import scala.util.Try

import org.parboiled2.Parser.DeliveryScheme.Either
import zio.ZIO

import space.littleinferno.schedule.domian.predef.{Auditorium, LessonFormat, ScheduleFormat, ScheduleKind}
import space.littleinferno.schedule.parser.types.{EntryReadError, ScheduleFF, ScheduleZ}
import space.littleinferno.schedule.parser.reader.utils._

class EntryReader() {

  import cats.implicits._

  def read(input: RawSchedule): ScheduleZ[ScheduleFormat] =
    ScheduleZ
      .flatTraversePar(input.lessons)(parseEntry)
      .parallelErrors
      .map(ScheduleFormat(input.group, _, ScheduleKind.Actual))

  def parseEntry(lesson: RawLesson): ScheduleFF[Seq[LessonFormat]] =
    for {
      lessonParser    <- EntryParser(lesson.lesson)
      classroomParser <- EntryParser(lesson.classroom)

      lessons <- ZIO
        .fromEither(
          lessonParser
            .all(lesson.number)
            .run()
            .leftMap(err => EntryReadError(lessonParser.formatError(err), lesson.x, lesson.y))
        )
      classrooms <- ZIO
        .fromEither(
          classroomParser.rooms
            .run()
            .leftMap(err => EntryReadError(classroomParser.formatError(err), lesson.x, lesson.y))
        )

      result <- tryMerge(lessons, classrooms, lesson.x, lesson.y)
    } yield result

  def tryMerge(lessons: Seq[Lesson], classrooms: Seq[Auditorium], x: Int, y: Int): ScheduleFF[Seq[LessonFormat]] =
    if (lessons.length == classrooms.length)
      ZIO.succeed(lessons.zip(classrooms).map {
        case (lesson, classroom) => lesson.makeLessonFormat(classroom)
      })
    else if (lessons.length < classrooms.length)
      ZIO.fail(EntryReadError("invalid lesson count", x, y))
    else ZIO.succeed(merge(lessons, classrooms))

  def merge(lessons: Seq[Lesson], classrooms: Seq[Auditorium]): Seq[LessonFormat] =
    lessons match {
      case head +: tail =>
        tail
          .foldLeft(Seq(head.makeLessonFormat(classrooms.head)), classrooms.tail) {
            case ((lessons, rooms), i) =>
              if (lessons.last.name == i.name || lessons.last.teacher == i.teacher)
                (lessons :+ i.makeLessonFormat(lessons.last.auditorium), rooms)
              else
                (
                  lessons :+ i.makeLessonFormat(rooms.headOption.getOrElse(Auditorium("", 0))),
                  Try(rooms.tail).toOption.getOrElse(Seq.empty)
                )
          }
          ._1
      case Seq(lesson) => Seq(lesson.makeLessonFormat(classrooms.headOption.getOrElse(Auditorium("", 0))))
    }
}

object EntryReader {
  def apply(): ScheduleZ[EntryReader] = ZIO.succeed(new EntryReader)
}
