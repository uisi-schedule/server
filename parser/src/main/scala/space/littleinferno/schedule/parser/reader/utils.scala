package space.littleinferno.schedule.parser.reader

import java.time.{DayOfWeek, LocalTime}

import cats.implicits._

import scala.util.matching.Regex
import space.littleinferno.schedule.domian.predef.{Auditorium, LessonFormat, LessonType, TimeSlot}

private[reader] object utils {
  private val nameExtractor: Regex = """([А-Я][а-я]+)\s*([А-Я].[А-Я].)$""".r
  private val lessonTypeExtractor: Regex =
    "(лаб.раб.)|(лаб.раб)|(практика)|(теория)|(лекция)|(ЗАЧЕТ с оценкой)|(ЗАЧЕТ С ОЦЕНКОЙ)|(диф.ЗАЧЕТ)|(ЗАЧЕТ)".r

  def extractTeacherName(fullName: String): String = {
    //TODO Fixme
    val name = nameExtractor.findAllIn(fullName.trim).matchData.toList.flatMap(_.subgroups).mkString(" ")
    require(name.nonEmpty, fullName)
    name
  }

  def isLessonType(str: String): Boolean =
    lessonTypeExtractor.findFirstIn(str.trim).isDefined

  def extractLessonType(str: String): LessonType =
    lessonTypeExtractor.findFirstIn(str) match {
      case Some("лаб.раб.") | Some("лаб.раб")                                                    => LessonType.Laboratory
      case Some("практика")                                                                      => LessonType.Practice
      case Some("теория") | Some("лекция")                                                       => LessonType.Lecture
      case Some("ЗАЧЕТ с оценкой") | Some("ЗАЧЕТ С ОЦЕНКОЙ") | Some("диф.ЗАЧЕТ") | Some("ЗАЧЕТ") => LessonType.Test
      case _                                                                                     => LessonType.Unknown
    }

  implicit class ToAddFormat(val lesson: Lesson) extends AnyVal {
    def makeLessonFormat(classroom: Auditorium): LessonFormat = LessonFormat(
      none,
      lesson.name,
      lesson.dates.toList,
      lesson.lessonType,
      lesson.subGroup,
      lesson.teacher,
      classroom,
      if (lesson.dates.head.getDayOfWeek == DayOfWeek.SATURDAY)
        saturdaySlots(lesson.number - 1)
      else
        timeSlots(lesson.number - 1)
    )
  }

  val timeSlots = TimeSlot(LocalTime.of(8, 30), LocalTime.of(10, 5), 1.some) ::
  TimeSlot(LocalTime.of(10, 15), LocalTime.of(11, 50), 2.some) ::
  TimeSlot(LocalTime.of(12, 35), LocalTime.of(14, 10), 3.some) ::
  TimeSlot(LocalTime.of(14, 20), LocalTime.of(15, 55), 4.some) ::
  TimeSlot(LocalTime.of(16, 5), LocalTime.of(17, 40), 5.some) ::
  TimeSlot(LocalTime.of(17, 50), LocalTime.of(19, 25), 6.some) :: Nil

  val saturdaySlots = TimeSlot(LocalTime.of(8, 30), LocalTime.of(10, 5), 1.some) ::
  TimeSlot(LocalTime.of(10, 15), LocalTime.of(11, 50), 2.some) ::
  TimeSlot(LocalTime.of(12, 0), LocalTime.of(13, 40), 3.some) ::
  TimeSlot(LocalTime.of(13, 50), LocalTime.of(15, 25), 4.some) ::
  Nil
}
