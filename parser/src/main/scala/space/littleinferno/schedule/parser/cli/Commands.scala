package space.littleinferno.schedule.parser.cli

import java.net.URI

import org.backuity.clist._
import os.{FilePath, Path, RelPath}

import space.littleinferno.schedule.parser.cli.instances._
import space.littleinferno.schedule.parser.config.ClientConfig
import space.littleinferno.schedule.parser.types.StartupContext
import space.littleinferno.schedule.parser.{FileInputSource, InputSource}

sealed trait ScheduleCommand { this: Command =>
  def getStartupContext(username: => String, password: => String): StartupContext
}

case class FromFiles() extends Command(description = "Read schedule from files or direcoties") with ScheduleCommand {
  var files: List[FilePath] = args[List[FilePath]](description = "files or folders with schedules")

  var username: Option[String] = opt[Option[String]](description = "user name")
  var password: Option[String] = opt[Option[String]](description = "user password")

  var host: Option[URI] = opt[Option[URI]](description = "host address")

  def getStartupContext(u: => String, p: => String): StartupContext =
    StartupContext(FileInputSource(files.map {
      case path: RelPath => os.pwd / path
      case path: Path    => path
    }), username.getOrElse(u), password.getOrElse(p), host.map(ClientConfig(_)))
}
