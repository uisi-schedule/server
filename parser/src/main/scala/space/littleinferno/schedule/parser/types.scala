package space.littleinferno.schedule.parser

import zio.ZIO
import zio.clock.Clock
import zio.console.Console

import space.littleinferno.schedule.parser.config.ClientConfig

object types {
  type AppEnvironment = Clock with Console
  type ScheduleFF[A]  = ZIO[AppEnvironment, ScheduleError, A]
  type ScheduleZ[A]   = ZIO[AppEnvironment, List[ScheduleError], A]

  object ScheduleZ {
    def fromOption[R, E, A](value: Option[A], error: E): ZIO[R, E, A] =
      value match {
        case Some(value) => ZIO.succeed(value)
        case None        => ZIO.fail(error)
      }

    def flatTraversePar[R, E, A, B](as: Iterable[A])(fn: A => ZIO[R, E, Iterable[B]]): ZIO[R, E, List[B]] =
      ZIO.traversePar(as)(fn).map(_.flatten)
  }

  case class StartupContext(inputSource: InputSource, username: String, password: String, host: Option[ClientConfig])

  sealed trait ScheduleError extends Throwable

  case class ExcelReadError(x: Int, y: Int, message: String) extends ScheduleError {
    override def toString: String = s"x:$x, y:$y $message"
  }

  case class EntryReadError(message: String, x: Int, y: Int) extends ScheduleError {
    override def toString: String = s"x:$x, y:$y $message"
  }

  case object StartupError extends ScheduleError

  case class NetworkError(message:String) extends ScheduleError

  case object InvalidCredentials extends ScheduleError

  case class OtherError(throwable: Throwable) extends ScheduleError {
    override def toString: String = throwable.toString
  }

}
