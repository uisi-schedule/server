package space.littleinferno.schedule.parser.cli

import org.backuity.clist.util.Read
import os.FilePath

object instances {
  implicit val pathRead: Read[FilePath] = Read.reads("a File")(FilePath(_))
}
