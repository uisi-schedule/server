package space.littleinferno.schedule.parser

import cats.instances.list._
import cats.syntax.traverse._
import zio.ZIO
import zio.console.putStrLn
import zio.interop.catz._
import space.littleinferno.schedule.domian.domian.{Credentials, Token, TokenPair}
import space.littleinferno.schedule.domian.instances._
import space.littleinferno.schedule.domian.predef.{Login, ScheduleFormat}
import space.littleinferno.schedule.parser.config.AppConfig
import space.littleinferno.schedule.parser.http.ScalajClient
import space.littleinferno.schedule.parser.reader.{EntryReader, ExcelReader}
import space.littleinferno.schedule.parser.types.ScheduleZ

class Application {
  def start(args: List[String]): ScheduleZ[List[ScheduleFormat]] =
    for {
      conf    <- AppConfig.load()
      startup <- cli.startup(args)
      _       <- putStrLn("Start...")
      http = new ScalajClient(startup.host.getOrElse(conf.host))
      _                       <- putStrLn("Check credentials")
      implicit0(token: TokenPair) <- http.login(Credentials(startup.username.coerce[Login], startup.password))
      _                       <- putStrLn("Ok")
      _                       <- putStrLn("Read schedules")
      raw                     <- new ExcelReader().read(startup.inputSource)
      _                       <- putStrLn("Ok")
      parser                  <- EntryReader()
      _                       <- putStrLn("Parse schedules")
      res                     <- ZIO.traverse(raw)(parser.read)
      _                       <- putStrLn("Ok")
      _                       <- putStrLn("Send schedules")
      _                       <- res.traverse(http.sendSchedule)
      _                       <- putStrLn("Ok")
      _                       <- putStrLn("Complete")
    } yield res
}
