package space.littleinferno.schedule.parser.reader

import scala.collection.JavaConverters._
import scala.util.Try

import cats.instances.option._
import cats.syntax.apply._
import cats.syntax.option._
import org.apache.poi.openxml4j.opc.OPCPackage
import org.apache.poi.ss.usermodel.{Cell, CellType, Row, Sheet}
import org.apache.poi.ss.util.SheetUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import zio.ZIO

import space.littleinferno.schedule.parser.InputSource
import space.littleinferno.schedule.parser.types._

class ExcelReader {
  import ExcelReader._

  def read[Input <: InputSource](input: Input): ScheduleZ[List[RawSchedule]] =
    ScheduleZ
      .flatTraversePar(input.input) { i =>
        ScheduleZ.flatTraversePar(new XSSFWorkbook(OPCPackage.open(i)).iterator.asScala.toList)(
          implicit sheet => readSheet
        )
      }
      .catchAllCause(h => ZIO.fail(h.failures))

  def readSheet(implicit sheet: Sheet): ScheduleFF[List[RawSchedule]] = {
    val headRow = sheet.iterator.asScala.dropHead.next.getRowNum
    ZIO.traversePar(2 to sheet.getScheduleCount(headRow) by 2) { column =>
      ScheduleZ
        .fromOption[AppEnvironment, ScheduleError, String](
          sheet.getCellValue(headRow, column),
          ExcelReadError(headRow, column, "Group mast have name")
        )
        .map(group => readSchedule(group, column, headRow + 1))
    }
  }

  def readSchedule(group: String, column: Int, row: Int)(implicit sheet: Sheet): RawSchedule =
    RawSchedule(
      group,
      (for {
        r      <- row to sheet.getLastRowNum
        number <- sheet.getCellValue(r, 1)
        lesson    = sheet.getCellValue(r, column)
        classroom = sheet.getCellValue(r, column + 1, true)
        lesson <- (Try(number.toFloat.toInt).toOption, lesson, classroom).mapN {
          case (number, lesson, classroom) =>
            RawLesson(number, lesson, classroom, column, r)
        }
      } yield lesson).toList
    )
}

object ExcelReader {
  implicit class SheetOps(private val sheet: Sheet) extends AnyVal {
    def getScheduleCount(headRow: Int): Int =
      sheet.getRow(headRow).getLastCellNum - 1

    def getCellValue(row: Int, column: Int, classroom: Boolean = false): Option[String] =
      getCell(row, column, classroom)
        .flatMap(
          cell =>
            cell.getCellType match {
              case CellType.STRING  => cell.getStringCellValue.some
              case CellType.NUMERIC => cell.getNumericCellValue.toString.some
              case _                => none[String]
          }
        )

    private def getCell(row: Int, column: Int, classroom: Boolean): Option[Cell] =
      sheet.getMergedRegions.asScala
        .find(_.isInRange(row, column))
        .flatMap(
          range =>
            Option(sheet.getRow(range.getFirstRow))
              .flatMap(
                row =>
                  Option(
                    row.getCell(
                      if (classroom)
                        range.getLastColumn + 1
                      else
                        range.getFirstColumn
                    )
                )
            )
        ) match {
        case Some(value) => value.some
        case None        => Option(SheetUtil.getCell(sheet, row, column))
      }
  }

  implicit class IteratorOps(iterator: Iterator[Row]) {
    def dropHead: Iterator[Row] =
      iterator.dropWhile { row =>
        val cell = row.getCell(0)
        cell.getCellType match {
          case CellType.STRING =>
            !cell.getStringCellValue.startsWith("дни")
          case _ =>
            true
        }
      }
  }
}
