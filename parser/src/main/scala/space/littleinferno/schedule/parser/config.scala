package space.littleinferno.schedule.parser

import java.net.URI

import io.circe.Decoder
import io.circe.config.parser
import org.manatki.derevo.circeDerivation.decoder
import org.manatki.derevo.derive
import zio.ZIO

import space.littleinferno.schedule.parser.types.{OtherError, ScheduleZ}

object config {
  implicit def uriDecoder: Decoder[URI] = Decoder.decodeString.map(URI.create)

  @derive(decoder)
  case class ClientConfig(address: URI)

  @derive(decoder)
  case class AppConfig(host: ClientConfig)

  object AppConfig {
    def load(): ScheduleZ[AppConfig] =
      ZIO.fromEither(parser.decode[AppConfig]()).mapError(err => List(OtherError(err)))
  }
}
