package space.littleinferno.schedule.parser

import java.time.{LocalDate, Period}

import scala.collection.JavaConverters._

import space.littleinferno.schedule.domian.predef.{Auditorium, LessonType}

package object reader {
  sealed trait ScheduleDate {
    def localDates: Seq[LocalDate]
  }

  final case class Day(day: Int) {
    def date(year: Int, month: Int): LocalDate =
      LocalDate.of(2000 + year, month, day)
  }

  final case class Month(days: Seq[Day], month: Int) {
    def dates(year: Int): Seq[LocalDate] = days.map(_.date(year, month))
  }

  final case class Date(months: Seq[Month], value: Int) extends ScheduleDate {
    val localDates: Seq[LocalDate] = months.flatMap(_.dates(value))
  }

  final case class DatePeriod(begin: Date, end: Date) extends ScheduleDate {
    val localDates: Seq[LocalDate] = {
      begin.localDates.zip(end.localDates).flatMap {
        case (begin, end) =>
          begin
            .datesUntil(end.plusDays(7), Period.ofDays(7))
            .iterator
            .asScala
            .toList
      }
    }
  }

  final case class DateList(dates: Seq[ScheduleDate]) extends ScheduleDate {
    val localDates: Seq[LocalDate] = dates.flatMap(_.localDates)
  }

  final case class DateType(dates: ScheduleDate, lessonType: LessonType, subGroup: Option[Int])

  final case class DateSubGroup(dates: ScheduleDate, subGroup: Int)

  final case class SubGroupTeacher(subGroup: Int, teacher: String)

  sealed trait LessonPart

  final case class DateSubGroupTeacher(dates: ScheduleDate, subGroup: Int, teacher: String) extends LessonPart

  final case class DateTypesSubGroupTeacher(
      dates: ScheduleDate,
      lessonType: LessonType,
      subGroup: Option[Int],
      teacher: String
  ) extends LessonPart

  case class RawLesson(number: Int, lesson: String, classroom: String, x: Int, y: Int)

  case class RawSchedule(group: String, lessons: List[RawLesson])

  case class Lesson(
      number: Int,
      name: String,
      dates: Seq[LocalDate],
      lessonType: LessonType,
      subGroup: Option[Int],
      teacher: String
  )

  trait Classroom {
    def rooms: Seq[Auditorium]
  }

  final case class RawAuditorium(room: Seq[Int], building: Int) extends Classroom {
    override def rooms: Seq[Auditorium] = room.map(number => Auditorium(number.toString, building))
  }

  final case class RomanAuditorium(rim: String) extends Classroom {
    override def rooms: Seq[Auditorium] = Seq(Auditorium(rim, RomanAuditorium.building(rim)))
  }

  object RomanAuditorium {
    def building(name: String) = name match {
      case "I" | "III" | "IV"          => 5
      case "II"                        => 4
      case "V" | "VI" | "VII" | "VIII" => 3

    }
  }

  final case class StubAuditorium(stub: Seq[String]) extends Classroom {
    override def rooms: Seq[Auditorium] = stub.filter(_ == "с/з").map(n => Auditorium(n, 1))
  }

  final case class FullLesson(
      number: Int,
      name: String,
      dates: Seq[LocalDate],
      lessonType: LessonType,
      subGroup: Option[Int],
      teacher: String,
      classroom: String
  )

  object FullLesson {
    def apply(lesson: Lesson, classroom: String): FullLesson =
      new FullLesson(
        lesson.number,
        lesson.name,
        lesson.dates,
        lesson.lessonType,
        lesson.subGroup,
        lesson.teacher,
        classroom
      )
  }

}
