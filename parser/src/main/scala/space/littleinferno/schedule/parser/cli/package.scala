package space.littleinferno.schedule.parser

import scala.io.StdIn

import org.backuity.clist.Cli

import space.littleinferno.schedule.parser.types._

package object cli {
  def startup(args: List[String]): ScheduleZ[StartupContext] =
    ScheduleZ.fromOption[AppEnvironment, List[ScheduleError], StartupContext](
      Cli
        .parse(args.toArray)
        .version(BuildInfo.version)
        .throwExceptionOnError()
        .withProgramName(BuildInfo.name)
        .withCommand(FromFiles()) { command =>
          command.getStartupContext(getUsername, readPassword)
        },
      List(StartupError)
    )

  private def getUsername: String =
    StdIn.readLine("Enter username: ")

  private def readPassword: String =
    StdIn.readLine("Enter password: ")

}
