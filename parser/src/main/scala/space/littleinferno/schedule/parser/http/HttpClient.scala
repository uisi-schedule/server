package space.littleinferno.schedule.parser.http

import io.circe.parser._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}
import scalaj.http._
import zio.ZIO

import space.littleinferno.schedule.domian.domian
import space.littleinferno.schedule.domian.domian.Credentials
import space.littleinferno.schedule.domian.predef.ScheduleFormat
import space.littleinferno.schedule.parser.config.ClientConfig
import space.littleinferno.schedule.parser.types.{InvalidCredentials, NetworkError, OtherError, ScheduleZ}

trait HttpClient {
  def sendSchedule(schedule: ScheduleFormat)(implicit token: domian.TokenPair): ScheduleZ[Unit]
}

class ScalajClient(config: ClientConfig) extends HttpClient {
  def sendSchedule(schedule: ScheduleFormat)(implicit token: domian.TokenPair): ScheduleZ[Unit] =
    sendJson[ScheduleFormat, Unit](
      config.address + "/schedules",
      schedule,
      Map("authorization" -> s"Bearer ${token.access}")
    )

  private def sendJson[In: Encoder, Out: Decoder](
      url: String,
      body: In,
      headers: Map[String, String] = Map.empty
  ): ScheduleZ[Out] =
    for {
      response <- ZIO
        .effect(
          Http(url)
            .headers(headers + ("content-type" -> "application/json"))
            .postData(body.asJson.noSpaces)
            .asString
        )
        .mapError(err => List(OtherError(err)))
      res <- if (response.body == "\"invalid credentials\"")
        ZIO.fail(List(InvalidCredentials))
      else if (response.isSuccess)
        ZIO
          .fromEither(parse(response.body))
          .flatMap(json => ZIO.fromEither(json.as[Out]))
          .mapError(t => List(OtherError(t)))
      else
        ZIO.fail(List(NetworkError(response.body)))
    } yield res

  def login(credentials: Credentials): ScheduleZ[domian.TokenPair] =
    sendJson[Credentials, domian.TokenPair](config.address + "/auth/login", credentials)

}
