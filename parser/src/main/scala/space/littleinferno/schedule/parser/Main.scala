package space.littleinferno.schedule.parser

import zio.console._
import zio.{App, Cause, ZIO}

import space.littleinferno.schedule.parser.types.{AppEnvironment, ScheduleError}

object Main extends App {

  def run(args: List[String]): ZIO[AppEnvironment, Nothing, Int] =
    (for {
      app <- ZIO.succeed(new Application())
      res <- app
        .start(args)
        .foldCauseM(errs, _ => putStrLn("End parse"))
    } yield res).fold(_ => 1, _ => 0)

  def errs(cause: Cause[List[ScheduleError]]): ZIO[Console, Nothing, Unit] =
    for {
      _ <- ZIO.traverse(cause.failures)(e => putStrLn(e.map(_.toString).mkString("\n---\n")))
    } yield ()
}
