package space.littleinferno.schedule.parser

import java.io.{FileInputStream, InputStream}

import org.apache.commons.io.FilenameUtils
import os.Path

trait InputSource {
  def input: Stream[InputStream]
}

case class FileInputSource(paths: List[Path]) extends InputSource {
  override def input: Stream[InputStream] =
    paths.toStream
      .map(_.toIO)
      .flatMap(file => if (file.isDirectory) file.listFiles else Stream(file))
      .filter(file => FilenameUtils.getExtension(file.getName) == "xlsx")
      .map(new FileInputStream(_))
}
