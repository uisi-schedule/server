FROM boxfuse/flyway:5.2.4

COPY server/src/main/resources/db/migration /flyway/sql

WORKDIR /flyway

ENTRYPOINT ["flyway"]