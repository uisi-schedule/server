package space.littleinferno.schedule.domian

import java.time.{LocalDate, LocalTime}

import enumeratum.CirceEnum
import io.circe.generic.extras.{Configuration, ConfiguredJsonCodec}
import io.estatico.newtype.NewType
import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.{SwaggerPrimitive, SwaggerType, SwaggerTypeable}
import ru.tinkoff.tschema.swagger.SwaggerTypeable.SwaggerTypeableEnum

import scala.collection.immutable

object predef {
  implicit val codec: Configuration = Configuration.default.withDiscriminator("type")

  implicit val localDateSwagger: SwaggerTypeable[LocalTime] = {
    new SwaggerTypeable[LocalTime] {
      override def typ: SwaggerType = SwaggerPrimitive.dateTime //FIX ME
    }
  }

  type EnumEntry = enumeratum.EnumEntry
  val EnumEntry = enumeratum.EnumEntry

  trait Enum[A <: EnumEntry]
    extends enumeratum.Enum[A] with CirceEnum[A] with SwaggerTypeableEnum[A] with HttpParam.Enum[A]

  val Enum = enumeratum.Enum

  sealed trait LessonType extends EnumEntry
  object LessonType extends Enum[LessonType] {
    def values: immutable.IndexedSeq[LessonType] = findValues

    case object Lecture    extends LessonType
    case object Practice   extends LessonType
    case object Laboratory extends LessonType
    case object Test       extends LessonType
    case object Exam       extends LessonType
    case object Action     extends LessonType
    case object Unknown    extends LessonType
  }

  sealed trait ScheduleKind extends EnumEntry
  object ScheduleKind extends Enum[ScheduleKind] {
    def values: immutable.IndexedSeq[ScheduleKind] = findValues

    case object Draft   extends ScheduleKind
    case object Actual  extends ScheduleKind
    case object Archive extends ScheduleKind
  }

  @derive(encoder, decoder, swagger)
  case class TimeSlot(begin: LocalTime, end: LocalTime, number: Option[Int])

  @derive(encoder, decoder, swagger)
  case class Auditorium(number: String, building: Int)

  @derive(encoder, decoder, swagger)
  case class LessonFormat(
      id: Option[Int],
      name: String,
      dates: List[LocalDate],
      lessonType: LessonType,
      subGroup: Option[Int],
      teacher: String,
      auditorium: Auditorium,
      timeSlot: TimeSlot
  )

  @derive(encoder, decoder, swagger)
  case class ScheduleFormat(group: String, lessons: List[LessonFormat], kind: ScheduleKind)

  @derive(swagger)
  @ConfiguredJsonCodec
  sealed trait EditScheduleAction

  object EditScheduleAction {
    case class Add(lesson: LessonFormat) extends EditScheduleAction

    case class Edit(id: Int, lesson: LessonFormat) extends EditScheduleAction

    case class Delete(id: Int) extends EditScheduleAction
  }

  type Login = Login.Type
  object Login extends NewType.Default[String]
}
