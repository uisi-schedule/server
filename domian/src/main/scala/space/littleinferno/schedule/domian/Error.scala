package space.littleinferno.schedule.domian

trait Error {
  val message: String
}

final case class TokenError() extends Error {
  val message: String = "Invalid token"
}