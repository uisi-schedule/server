package space.littleinferno.schedule.domian

import java.time.LocalDate

import io.circe.{Decoder, Encoder}
import io.estatico.newtype.{Coercible, ToCoercibleIdOps}
import io.estatico.newtype.ops.ToNewTypeOps
import ru.tinkoff.tschema.swagger.SwaggerTypeable

object instances extends NewTypeInstances

trait NewTypeInstances extends ToNewTypeOps with ToCoercibleIdOps {
  implicit def newTypeEncoder[R, N](implicit ev: Coercible[Encoder[R], Encoder[N]], R: Encoder[R]): Encoder[N] =
    ev(R)

  implicit def newTypeDecoder[R, N](implicit ev: Coercible[Decoder[R], Decoder[N]], R: Decoder[R]): Decoder[N] =
    ev(R)

  implicit def newTypeSwagger[R, N](
      implicit ev: Coercible[SwaggerTypeable[R], SwaggerTypeable[N]],
      R: SwaggerTypeable[R]
  ): SwaggerTypeable[N] =
    ev(R)
}

trait JsonInstances {
  implicit val localDateEncoder: Encoder[LocalDate] = Encoder.encodeString.contramap(_.toString)
  implicit val localDateDecoder: Decoder[LocalDate] = Decoder.decodeString.map(LocalDate.parse)
}
