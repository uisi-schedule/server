package space.littleinferno.schedule.domian

import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

import space.littleinferno.schedule.domian.instances._
import space.littleinferno.schedule.domian.predef.Login

object domian {
  @derive(decoder, encoder, swagger)
  case class Credentials(login: Login, password: String)

  @derive(encoder, decoder, swagger)
  final case class Session(login: Login)

  @derive(encoder, decoder, swagger)
  final case class Token(token: String)

  @derive(encoder, decoder, swagger)
  final case class TokenPair(access: String, refresh: String)
}
