# UISI Schedule

Because using schedule in [UISI](http://www.uisi.ru/) should be easy.

(╯°□°）╯︵ ┻━┻


# Сервер

## Перед запуском

Для запуска сервера нужна подготовленная база данных PostgreSQL
создайте базу данных, и с помощью [flyway](https://flywaydb.org/)
подготовьте базу:
```
$ flyway migrate -url=jdbc:postgresql://localhost:5432/DB_NAME  -user=DB_USER -password=DB_PASSWORD -locations=filesystem:server/src/main/resources/db/migration
```

Создайте файл bootstrap.conf:
```
bootstrap {
  login = FIRST_USER_LOGIN
  password = FIRST_USER_PASSWORD
}
```
добавтьте переменную окружения override=PATH_TO_BOOTSTRAP_CONF
Запустите сервер.
Удалите переменную override. Перезапустите сервер.
