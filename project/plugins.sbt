addSbtPlugin("com.typesafe.sbt"  % "sbt-native-packager" % "1.3.25")
addSbtPlugin("com.eed3si9n"      % "sbt-buildinfo"       % "0.9.0")
addSbtPlugin("se.marcuslonnberg" % "sbt-docker"          % "1.5.0")
addSbtPlugin("com.typesafe.sbt"  % "sbt-git"             % "1.0.0")

addSbtPlugin("org.jmotor.sbt" % "sbt-dependency-updates" % "1.2.1")
