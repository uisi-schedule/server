import sbt._

object Dependencies extends common.Deps with test.Deps

object common {
  object version {

    object circe {
      lazy val core    = "0.12.3"
      lazy val config  = "0.7.0"
      lazy val generic = "0.12.2"
    }

    object enumeratum {
      val core  = "1.5.15"
      val circe = "1.5.22"
    }

    object cats {
      val core   = "2.1.0"
      val effect = "2.0.0"
    }

    val derevo = "0.10.5"

    val doobie = "0.8.8"

    val osLib = "0.3.0"

    val apacheIO = "2.6"

    lazy val typedSchedma = "0.11.0-RC1"

    lazy val newtype = "0.4.3"

    lazy val apachePoi = "4.1.1"

    lazy val parboiled = "2.1.8"

    lazy val scalajHttp = "2.4.2"

    lazy val jose = "0.3.0.1-M1"

    lazy val swaggerUI = "3.23.10"

    lazy val bcrypt = "4.1"

    lazy val zio = new {
      lazy val core = "1.0.0-RC13"
      lazy val cats = "2.0.0.0-RC3"
    }

    lazy val akkaCirce = "1.29.1"

    lazy val clist = "3.5.1"
  }

  trait Deps {
    object circe {
      lazy val core    = "io.circe" %% "circe-core"           % version.circe.core
      lazy val generic = "io.circe" %% "circe-generic-extras" % version.circe.generic
      lazy val parser  = "io.circe" %% "circe-parser"         % version.circe.core
      lazy val config  = "io.circe" %% "circe-config"         % version.circe.config
    }

    object enumeratum {
      lazy val core  = "com.beachape" %% "enumeratum"       % version.enumeratum.core
      lazy val circe = "com.beachape" %% "enumeratum-circe" % version.enumeratum.circe
    }

    object cats {
      lazy val core   = "org.typelevel" %% "cats-core"   % version.cats.core
      lazy val effect = "org.typelevel" %% "cats-effect" % version.cats.effect
    }

    object derevo {
      lazy val core    = "org.manatki" %% "derevo-core"    % version.derevo
      lazy val circe   = "org.manatki" %% "derevo-circe"   % version.derevo
      lazy val tschema = "org.manatki" %% "derevo-tschema" % version.derevo
    }

    object doobie {
      lazy val core     = "org.tpolecat" %% "doobie-core"     % version.doobie
      lazy val postgres = "org.tpolecat" %% "doobie-postgres" % version.doobie
    }

    object apache {
      lazy val poi         = "org.apache.poi" % "poi"       % version.apachePoi
      lazy val `poi-ooxml` = "org.apache.poi" % "poi-ooxml" % version.apachePoi
    }

    object jose {
      lazy val core  = "black.door" %% "jose"            % version.jose
      lazy val circe = "black.door" %% "jose-json-circe" % version.jose
    }

    lazy val zio = new {
      lazy val core = "dev.zio" %% "zio"              % version.zio.core
      lazy val cats = "dev.zio" %% "zio-interop-cats" % version.zio.cats
    }

    object clist {
      lazy val core   = "org.backuity.clist" %% "clist-core"   % version.clist
      lazy val macros = "org.backuity.clist" %% "clist-macros" % version.clist % "provided"
    }

    lazy val osLib        = "com.lihaoyi" %% "os-lib"       % version.osLib
    lazy val newtype      = "io.estatico" %% "newtype"      % version.newtype
    lazy val typedSchedma = "ru.tinkoff"  %% "typed-schema" % version.typedSchedma
    lazy val apacheIO     = "commons-io"  % "commons-io"    % version.apacheIO

    lazy val parboiled = "org.parboiled" %% "parboiled" % version.parboiled

    lazy val scalajHttp = "org.scalaj" %% "scalaj-http" % version.scalajHttp

    lazy val swaggerUI = "org.webjars.npm" % "swagger-ui-dist" % version.swaggerUI

    lazy val bcrypt = "com.github.t3hnar" %% "scala-bcrypt" % version.bcrypt

    lazy val akkaCirce = "de.heikoseeberger" %% "akka-http-circe" % version.akkaCirce
  }
}

object test {
  object version {
    lazy val verify         = "0.2.0"
    lazy val testcontainers = "0.34.2"
    lazy val postgresql     = "1.12.4"
    lazy val flyway         = "6.1.3"
  }

  trait Deps {
    lazy val verify         = Seq("com.eed3si9n.verify" %% "verify" % version.verify)
    lazy val testcontainers = "com.dimafeng" %% "testcontainers-scala" % version.testcontainers % "it"
    lazy val postgresql     = "org.testcontainers" % "postgresql" % version.postgresql % "it"
    lazy val flyway         = "org.flywaydb" % "flyway-core" % version.flyway % "it"
    lazy val hikari         = "com.zaxxer" % "HikariCP" % "3.4.1" % "it"
  }
}
